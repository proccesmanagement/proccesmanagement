﻿using ProcessManagement.DTO.Envelopes.Base;
using ProcessManagementModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace ProcessManagement.DTO.Envelopes
{
    [DataContract]
    public class SignInRS : BaseRS
    {
        [DataMember]
        public Users User { get; set; }
    }
}
