﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace ProcessManagement.DTO.Envelopes.Base
{
    [DataContract]
    public class BaseRQ
    {
        [DataMember]
        public string ConnectionString { get; set; }
    }
}
