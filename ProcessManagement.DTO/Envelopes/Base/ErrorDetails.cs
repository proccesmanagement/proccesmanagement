﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace ProcessManagement.DTO.Envelopes.Base
{
    [DataContract]
    public class ErrorDetails
    {
        [DataMember]
        public string Message { get; set; }

        [DataMember]
        public string Source { get; set; }

        [DataMember]
        public DateTime DateTime { get; set; }
    }
}
