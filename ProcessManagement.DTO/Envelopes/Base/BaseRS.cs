﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace ProcessManagement.DTO.Envelopes.Base
{
    [DataContract]
    public class BaseRS
    {
        [DataMember]
        public bool Successful { get; set; }

        [DataMember]
        public List<ErrorDetails> ErrorsDetails { get; set; }

        public BaseRS()
        {
            Successful = true;
            ErrorsDetails = new List<ErrorDetails>();
        }
    }
}
