﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProcessManagement.DTO.ModelsView
{
    public class AlterPassword
    {
        public int UserId { get; set; }

        [Display(Name = "Senha atual")]
        [Required(ErrorMessage = "Senha atual é obrigatório")]
        [DataType(DataType.Password)]
        public string Password { get; set; }

        [Display(Name = "Nova senha")]
        [Required(ErrorMessage = "Nova senha é obrigatório")]
        [DataType(DataType.Password)]
        public string NewPassword { get; set; }

        [Display(Name = "Confirmar nova senha")]
        [Required(ErrorMessage = "Confirmar nova senha é obrigatório")]
        [DataType(DataType.Password)]
        [Compare("NewPassword")]
        public string ConfirmPassword { get; set; }
    }
}
