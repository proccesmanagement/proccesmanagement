﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProcessManagement.DTO.ModelsView
{
    public class User
    {
        public int Id { get; set; }

        [Display(Name = "Nome")]
        [Required(ErrorMessage = "Nome é obrigatório")]
        public string Name { get; set; }

        [Display(Name = "Usuário")]
        [Required(ErrorMessage = "Usuário é obrigatório")]
        public string UserName { get; set; }

        [Display(Name = "E-mail")]
        [Required(ErrorMessage = "E-mail é obrigatório")]
        [EmailAddress(ErrorMessage = "E-mail inválido")]
        public string Email { get; set; }

        [Display(Name = "Senha")]
        [DataType(DataType.Password)]
        public string Password { get; set; }

        public bool Active { get; set; }

        [Required(ErrorMessage = "Grupo de acesso é obrigatório")]
        public int? AccessGroupId { get; set; }

        public int? ProfileImageId { get; set; }
    }
}
