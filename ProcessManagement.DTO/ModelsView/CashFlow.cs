﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProcessManagement.DTO.ModelsView
{
    public class CashFlow
    {
        public CashFlow()
        {
            User = new User();
        }

        public int Id { get; set; }

        [Display(Name = "Tipo")]
        [Required(ErrorMessage = "Tipo é obrigatório")]
        public int TypeId { get; set; }

        [Display(Name = "Descrição")]
        [Required(ErrorMessage = "Descrição é obrigatório")]
        public string Description { get; set; }

        [Display(Name = "Valor total")]
        [Required(ErrorMessage = "Valor total é obrigatório")]
        [DisplayFormat(DataFormatString = "{0:0.00}", ApplyFormatInEditMode = true)]
        public  decimal TotalAmount { get; set; }

        [Display(Name = "Parcelas")]
        //[Required(ErrorMessage = "Parcelas é obrigatório")]
        public int Installments { get; set; }

        [Display(Name = "Valor da parcela")]
        [Required(ErrorMessage = "Valor parcelas é obrigatório")]
        [DisplayFormat(DataFormatString = "{0:0.00}", ApplyFormatInEditMode = true)]
        public decimal ValueInstallments { get; set; }

        [Display(Name = "Data pagamento")]
        [Required(ErrorMessage = "Data pagamento é obrigatório")]
        [DisplayFormat(DataFormatString = @"{0:dd\/MM\/yyyy}")]
        public DateTime FirstPaymentDate { get; set; }

        [Display(Name = "Detalhes")]
        public string Details { get; set; }

        public DateTime DateCreation { get; set; }

        public int UserId { get; set; }

        public User User { get; set; }

        public bool IsInput { get; set; }

        public virtual bool Active { get; set; }

        public List<ProcessManagementModel.CashFlowImages> CashFlowImages { get; set; }

        public List<ProcessManagementModel.CashFlowInstallments> CashFlowInstallments { get; set; }
    }
}
