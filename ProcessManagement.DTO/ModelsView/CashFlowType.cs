﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProcessManagement.DTO.ModelsView
{
    public class CashFlowType
    {
        public int Id { get; set; }

        [Display(Name = "Tipo")]
        [Required(ErrorMessage = "Tipo é obrigatório")]
        public int TypeId { get; set; }

        [Display(Name = "Nome")]
        [Required(ErrorMessage = "Nome é obrigatório")]
        public string Name { get; set; }

        [Display(Name = "Detalhes")]
        public string Details { get; set; }

        public bool Active { get; set; }
    }
}
