﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProcessManagement.DTO.ModelsView
{
    public class CashFlowInstallmentsDetails
    {
        public ProcessManagementModel.CashFlowInstallments CashFlowInstallment { get; set; }

        public List<ProcessManagementModel.Log> Logs { get; set; }
    }
}
