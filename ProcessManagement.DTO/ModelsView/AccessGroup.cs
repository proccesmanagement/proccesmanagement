﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ProcessManagement.DTO.ModelsView
{
    public class AccessGroup
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public bool Selected { get; set; }

        public bool Active { get; set; }

        public bool AssignResponsability { get; set; }

        public bool Approver { get; set; }
    }
}