﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProcessManagement.DTO.ModelsView
{
    public class Menu
    {
        public int Id { get; set; }

        public string Text { get; set; }

        public List<ModelsView.Menu> Menu_Childs { get; set; }

        public bool Selected { get; set; }

        public string Icon { get; set; }
    }
}
