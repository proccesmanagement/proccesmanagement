﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace ProcessManagement.DTO.ModelsView
{
    public class Image
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public string Path { get; set; }

        public int Size { get; set; }
    }
}
