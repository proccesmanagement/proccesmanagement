﻿//------------------------------------------------------------------------------
// This is auto-generated code.
//------------------------------------------------------------------------------
// This code was generated by Devart Entity Developer tool using Entity Framework DbContext template.
// Code is generated on: 25/04/2017 23:40:18
//
// Changes to this file may cause incorrect behavior and will be lost if
// the code is regenerated.
//------------------------------------------------------------------------------

using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;

namespace ProcessManagementModel
{

    /// <summary>
    /// There are no comments for ProcessManagementModel.CashFlowType in the schema.
    /// </summary>
    public partial class CashFlowType    {

        public CashFlowType()
        {
            OnCreated();
        }


        #region Properties
    
        /// <summary>
        /// There are no comments for Id in the schema.
        /// </summary>
        public virtual int Id
        {
            get;
            set;
        }

    
        /// <summary>
        /// There are no comments for Name in the schema.
        /// </summary>
        public virtual string Name
        {
            get;
            set;
        }

    
        /// <summary>
        /// There are no comments for Details in the schema.
        /// </summary>
        public virtual string Details
        {
            get;
            set;
        }

    
        /// <summary>
        /// There are no comments for TypeId in the schema.
        /// </summary>
        public virtual int TypeId
        {
            get;
            set;
        }

    
        /// <summary>
        /// There are no comments for Active in the schema.
        /// </summary>
        public virtual bool Active
        {
            get;
            set;
        }


        #endregion

        #region Navigation Properties
    
        /// <summary>
        /// There are no comments for CashFlow in the schema.
        /// </summary>
        public virtual ICollection<CashFlow> CashFlow
        {
            get;
            set;
        }
    
        /// <summary>
        /// There are no comments for Type in the schema.
        /// </summary>
        public virtual Type Type
        {
            get;
            set;
        }

        #endregion
    
        #region Extensibility Method Definitions
        partial void OnCreated();
        #endregion
    }

}
