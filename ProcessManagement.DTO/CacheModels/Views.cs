﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace ProcessManagement.DTO.CacheModels
{
    [DataContract]
    public class Views
    {
        [DataMember]
        public int Id { get; set; }

        [DataMember]
        public Controllers Controller { get; set; }

        [DataMember]
        public string ViewName { get; set; }

        [DataMember]
        public string Definition { get; set; }

        [DataMember]
        public bool Active { get; set; }
    }
}
