﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace ProcessManagement.DTO.CacheModels
{
    [DataContract]
    public class Menu
    {
        [DataMember]
        public int Id { get; set; }

        [DataMember]
        public Views View { get; set; }

        [DataMember]
        public string Text { get; set; }

        [DataMember]
        public string Icon { get; set; }

        [DataMember]
        public List<Menu> Menu_Childs { get; set; }

        [DataMember]
        public int? OrdemItem { get; set; }

        public Menu()
        {
            Menu_Childs = new List<Menu>();
        }
    }
}
