﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace ProcessManagement.DTO.CacheModels
{
    [DataContract]
    public class Controllers
    {
        [DataMember]
        public int Id { get; set; }

        [DataMember]
        public string Controller { get; set; }

        [DataMember]
        public bool Active { get; set; }
    }
}
