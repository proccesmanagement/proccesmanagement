﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;
using ProcessManagementModel;

namespace ProcessManagement.Core
{
    public class Log
    {
        public static void Create(ProcessManagementModel.ColumnTable column, string before, string after, int userId, int registerId, ProcessManagementEntities context)
        {
            context.Log.Add(new ProcessManagementModel.Log()
            {
                ColumnId = column.Id,
                Before = string.IsNullOrEmpty(before) ? string.Empty : before,
                After = string.IsNullOrEmpty(after) ? string.Empty : after,
                DateLog = DateTime.Now,
                UserId = userId,
                RegisterId = registerId
            });
        }

        public static ProcessManagementModel.Table GetTableByName(string tableName)
        {
            var rs = new ProcessManagementModel.Table();

            using (var context = new ProcessManagementModel.ProcessManagementEntities())
            {
                if (context.Table.Any(t => t.Name.ToLower().Equals(tableName.ToLower())))
                    rs = context.Table.Include(x => x.ColumnTable).Where(t => t.Name.ToLower().Equals(tableName.ToLower())).FirstOrDefault();
            }

            return rs;
        }
    }
}
