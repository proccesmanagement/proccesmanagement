﻿using ProcessManagement.DTO.Envelopes;
using ProcessManagement.DTO.Envelopes.Base;
using ProcessManagementModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;

namespace ProcessManagement.Core
{
    public class User
    {
        public static DTO.ModelsView.User Dettach(ProcessManagementModel.Users user)
        {
            return new DTO.ModelsView.User()
            {
                AccessGroupId = user.AccessGroupId,
                Active = user.Active,
                Email = user.Email,
                Id = user.Id,
                Name = user.Name,
                Password = user.Password,
                ProfileImageId = user.ProfileImageId,
                UserName = user.UserName
            };
        }

        public static List<ProcessManagementModel.Users> GetUsers(int clientId, int accessGroupId)
        {
            var permissions = GetAccessGroupPermissionsIds(accessGroupId);
            return Cache.GetUsers().Where(c => c.Active && c.AccessGroup.ClientId.Equals(clientId) && permissions.Contains(c.AccessGroupId)).ToList();
        }

        public static ProcessManagementModel.Users GetUserById(int id)
        {
            return Cache.GetUsers().Where(c => c.Id.Equals(id)).FirstOrDefault();
        }

        public static TransactionRS SaveUser(DTO.ModelsView.User user, Images image)
        {
            var rs = new TransactionRS();

            try
            {
                using (var context = new ProcessManagementEntities())
                {
                    if (user.Id.Equals(0))
                    {
                        if (context.Users.Any(u => u.UserName.Equals(user.UserName)))
                        {
                            rs.Successful = false;
                            rs.ErrorsDetails.Add(new ErrorDetails() { Message = "Nome de usuário ja está sendo usado. Digite outro nome de usuário" });
                            return rs;
                        }

                        int? imageId = null;

                        if (image.Content != null)
                            imageId = context.Images.Any(i => i.Content.Equals(image.Content)) ? context.Images.Where(i => i.Content.Equals(image.Content)).FirstOrDefault().Id : context.Images.Add(image).Id;

                        context.Users.Add(new Users()
                        {
                            Active = true,
                            AccessGroupId = user.AccessGroupId.Value,
                            Email = user.Email,
                            Name = user.Name,
                            Password = "1234",
                            ProfileImageId = imageId,
                            UserName = user.UserName
                        });

                        context.SaveChanges();
                        Core.Cache.ClearCache(new List<string>() { "Users", "Images" });
                    }
                    else
                    {
                        var userUpdated = context.Users.Find(user.Id);
                        userUpdated.AccessGroupId = user.AccessGroupId.Value;
                        userUpdated.Active = true;
                        userUpdated.Email = user.Email;
                        userUpdated.Name = user.Name;
                        if (image.Content != null)
                            userUpdated.ProfileImageId = context.Images.Any(i => i.Content.Equals(image.Content)) ? context.Images.Where(i => i.Content.Equals(image.Content)).FirstOrDefault().Id : context.Images.Add(image).Id;
                        else
                            userUpdated.ProfileImageId = null;
                        userUpdated.UserName = user.UserName;
                        context.Entry(userUpdated).State = System.Data.Entity.EntityState.Modified;

                        context.SaveChanges();
                        Core.Cache.ClearCache(new List<string>() { "Users", "Images" });
                    }
                }

                rs.Successful = true;
            }
            catch (Exception ex)
            {
                rs.Successful = false;
                rs.ErrorsDetails.Add(new ErrorDetails()
                {
                    Message = SystemSettings.RegexReplaceString(ex.Message),
                    DateTime = DateTime.Now
                });
            }

            return rs;
        }

        public static TransactionRS AlterPassword(DTO.ModelsView.AlterPassword password)
        {
            var rs = new TransactionRS();

            try
            {
                using (var context = new ProcessManagementEntities())
                {
                    var user = context.Users.Find(password.UserId);

                    if (password.Password != user.Password)
                    {
                        rs.Successful = false;
                        rs.ErrorsDetails.Add(new ErrorDetails() { Message = "Senha atual incorreta." });
                        return rs;
                    }

                    user.Password = password.NewPassword;
                    context.Entry(user).State = System.Data.Entity.EntityState.Modified;
                    context.SaveChanges();
                    Core.Cache.ClearCache(new List<string>() { "Users" });
                }

                rs.Successful = true;
            }
            catch (Exception ex)
            {
                rs.Successful = false;
                rs.ErrorsDetails.Add(new ErrorDetails()
                {
                    Message = SystemSettings.RegexReplaceString(ex.Message),
                    DateTime = DateTime.Now
                });
            }

            return rs;
        }

        public static ProcessManagementModel.Images GetProfileImage(int userId)
        {
            return Cache.GetUsers().Where(u => u.Id.Equals(userId)).FirstOrDefault().Images;
        }

        public static List<ProcessManagementModel.AccessGroup> GetAccessGroups(int clientId)
        {
            return Cache.GetAccessGroup().Where(g => g.ClientId.Equals(clientId) && g.Active).ToList();
        }

        public static List<int> GetAccessGroupPermissionsIds(int accessGroupId)
        {
            return Cache.GetAccessGroupPermissions().Where(a => a.AccessGroupId.Equals(accessGroupId)).Select(x => x.AccessGroupPermissionId).ToList();
        }

        public static List<ProcessManagement.DTO.ModelsView.AccessGroup> GetAccessGroupsPermissions(int accessGroupId)
        {
            var groups = new List<ProcessManagement.DTO.ModelsView.AccessGroup>();

            var groupsPermissions = Cache.GetAccessGroupPermissions().Where(a => a.AccessGroupId.Equals(accessGroupId)).ToList();

            groupsPermissions.ForEach(g => groups.Add(new DTO.ModelsView.AccessGroup()
            {
                Id = g.AccessGroupPermissionId,
                Name = g.AccessGroup_AccessGroupId.Name,
                Selected = true,
                Active = true,
                AssignResponsability = g.AccessGroup_AccessGroupId.AssignResponsability
            }));

            return groups;
        }

        public static ProcessManagement.DTO.ModelsView.AccessGroup GetAccessGroupById(int id)
        {
            var accessGroup = Cache.GetAccessGroup().Where(a => a.Id.Equals(id)).FirstOrDefault();

            return new ProcessManagement.DTO.ModelsView.AccessGroup()
            {
                Active = accessGroup.Active,
                Id = accessGroup.Id,
                Name = accessGroup.Name,
                AssignResponsability = accessGroup.AssignResponsability,
                Approver = accessGroup.Approver
            };
        }

        public static TransactionRS SaveAccessGroup(ProcessManagementModel.AccessGroup group, List<string> accessGroupsIds, List<string> menusIds, int clientId)
        {
            var rs = new TransactionRS();

            try
            {
                using (var context = new ProcessManagementEntities())
                {
                    if (group.Id.Equals(0))
                    {
                        group.Active = true;
                        group.ClientId = clientId;
                        var groupAdd = context.AccessGroup.Add(group);

                        foreach (var id in accessGroupsIds.Where(x => !x.Equals("")))
                        {
                            var groupPermissions = new ProcessManagementModel.AccessGroupPermissions();
                            groupPermissions.AccessGroupId = groupAdd.Id;
                            groupPermissions.AccessGroupPermissionId = id.Equals("0") ? groupPermissions.Id : Convert.ToInt32(id);

                            context.AccessGroupPermissions.Add(groupPermissions);
                        }

                        foreach (var id in menusIds)
                        {
                            var menuPermission = new ProcessManagementModel.AccessGroupMenuPermissions();
                            menuPermission.AccessGroupId = groupAdd.Id;
                            menuPermission.MenuId = Convert.ToInt32(id);

                            context.AccessGroupMenuPermissions.Add(menuPermission);
                        }
                    }
                    else
                    {
                        var groupUpdated = context.AccessGroup.Find(group.Id);
                        groupUpdated.Name = group.Name;
                        groupUpdated.AssignResponsability = group.AssignResponsability;
                        groupUpdated.Approver = group.Approver;
                        context.Entry(groupUpdated).State = System.Data.Entity.EntityState.Modified;

                        for (var i = 0; i < accessGroupsIds.Count; i++)
                            if (accessGroupsIds[i].Equals("0"))
                                accessGroupsIds[i] = groupUpdated.Id.ToString();

                        var groupsPermissionDeleted = context.AccessGroupPermissions.Where(a => a.AccessGroupId.Equals(group.Id) && !accessGroupsIds.Contains(a.AccessGroupPermissionId.ToString())).ToList();
                        foreach (var groupPermissionDeleted in groupsPermissionDeleted)
                            context.AccessGroupPermissions.Remove(groupPermissionDeleted);

                        foreach (var id in accessGroupsIds)
                        {
                            var groupPermissionId = Convert.ToInt32(id);
                            if (!context.AccessGroupPermissions.Any(a => a.AccessGroupId.Equals(groupUpdated.Id) && a.AccessGroupPermissionId.Equals(groupPermissionId)))
                            {
                                var groupPermissions = new ProcessManagementModel.AccessGroupPermissions();
                                groupPermissions.AccessGroupId = groupUpdated.Id;
                                groupPermissions.AccessGroupPermissionId = groupPermissionId;

                                context.AccessGroupPermissions.Add(groupPermissions);
                            }
                        }

                        var menusPermissionDeleted = context.AccessGroupMenuPermissions.Where(a => a.AccessGroupId.Equals(group.Id) && !menusIds.Contains(a.MenuId.ToString())).ToList();

                        foreach (var menuPermissionDeleted in menusPermissionDeleted)
                            context.AccessGroupMenuPermissions.Remove(menuPermissionDeleted);

                        foreach (var id in menusIds)
                        {
                            var menuPermissionId = Convert.ToInt32(id);
                            if (!context.AccessGroupMenuPermissions.Any(a => a.AccessGroupId.Equals(groupUpdated.Id) && a.MenuId.Equals(menuPermissionId)))
                            {
                                var menuPermission = new ProcessManagementModel.AccessGroupMenuPermissions();
                                menuPermission.AccessGroupId = groupUpdated.Id;
                                menuPermission.MenuId = menuPermissionId;

                                context.AccessGroupMenuPermissions.Add(menuPermission);
                            }
                        }
                    }

                    context.SaveChanges();
                    Core.Cache.ClearCache(new List<string>() { "AccessGroupPermissions", "AccessGroup", "AccessGroupMenuPermissions" });
                    rs.Successful = true;
                }
            }
            catch (Exception ex)
            {
                rs.Successful = false;
                rs.ErrorsDetails.Add(new ErrorDetails()
                {
                    Message = SystemSettings.RegexReplaceString(ex.Message),
                    DateTime = DateTime.Now
                });
            }

            return rs;
        }

        public static bool AccessGroupIsAuthority(int id)
        {
            return Cache.GetAccessGroup().Where(a => a.Id.Equals(id)).FirstOrDefault().AssignResponsability;
        }

        public static TransactionRS DeleteUser(int id)
        {
            var rs = new TransactionRS();

            try
            {
                using (var context = new ProcessManagementEntities())
                {
                    var user = context.Users.Find(id);
                    user.Active = false;

                    context.Entry(user).State = System.Data.Entity.EntityState.Modified;
                    context.SaveChanges();
                    Core.Cache.ClearCache(new List<string>() { "Users", "Images" });
                }

                rs.Successful = true;
            }
            catch (Exception ex)
            {
                rs.Successful = false;
                rs.ErrorsDetails.Add(new ErrorDetails()
                {
                    Message = SystemSettings.RegexReplaceString(ex.Message),
                    DateTime = DateTime.Now
                });
            }

            return rs;
        }

        public static TransactionRS DeleteAccessGroup(int id)
        {
            var rs = new TransactionRS();

            try
            {
                using (var context = new ProcessManagementEntities())
                {
                    var accessGroup = context.AccessGroup.Find(id);
                    accessGroup.Active = false;

                    context.Entry(accessGroup).State = System.Data.Entity.EntityState.Modified;
                    context.SaveChanges();
                    Core.Cache.ClearCache(new List<string>() { "AccessGroupPermissions", "AccessGroup", "AccessGroupMenuPermissions" });
                }

                rs.Successful = true;
            }
            catch (Exception ex)
            {
                rs.Successful = false;
                rs.ErrorsDetails.Add(new ErrorDetails()
                {
                    Message = SystemSettings.RegexReplaceString(ex.Message),
                    DateTime = DateTime.Now
                });
            }

            return rs;
        }

        public static List<ProcessManagement.DTO.ModelsView.Menu> GetMenuPermissions(int id)
        {
            var menus = new List<ProcessManagement.DTO.ModelsView.Menu>();

            var menuPermission = Cache.GetAccessGroupMenuPermissions().Where(a => a.AccessGroupId.Equals(id)).ToList();

            menuPermission.ForEach(m => menus.Add(new DTO.ModelsView.Menu()
            {
                Id = m.MenuId,
                Text = m.Menu.Text,
                Selected = true
            }));

            return menus;
        }

        public static bool MyData(int accessGroupId)
        {
            return !Cache.GetAccessGroupPermissions().Any(a => a.AccessGroupId.Equals(accessGroupId));
        }

        public static bool MyGroup(int accessGroupId)
        {
            return Cache.GetAccessGroupPermissions().Any(a => a.AccessGroupId.Equals(accessGroupId) && a.AccessGroupPermissionId.Equals(accessGroupId));
        }
    }
}
