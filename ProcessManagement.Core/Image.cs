﻿using ProcessManagementModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProcessManagement.Core
{
    public class Image
    {
        public static ProcessManagementModel.Images GetById(int id)
        {
            return Cache.GetImages().Where(i => i.Id.Equals(id)).FirstOrDefault();
        }
    }
}
