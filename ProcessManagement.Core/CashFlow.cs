﻿using ProcessManagement.DTO.Envelopes;
using ProcessManagement.DTO.Envelopes.Base;
using ProcessManagementModel;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;

namespace ProcessManagement.Core
{
    public class CashFlow
    {
        private static string Source = "ProcessManagement.Core.CashFlow";

        public static DTO.ModelsView.CashFlow InstallmentsToCashFlowView(CashFlowInstallments cashFlowInstallments)
        {
            return new DTO.ModelsView.CashFlow()
            {
                CashFlowImages = cashFlowInstallments.CashFlowImages.ToList(),
                DateCreation = cashFlowInstallments.CashFlow_CashFlowId.DateCreation,
                Description = cashFlowInstallments.Description,
                Details = cashFlowInstallments.Details,
                FirstPaymentDate = cashFlowInstallments.PaymentDate,
                Id = cashFlowInstallments.Id,
                TotalAmount = cashFlowInstallments.ValueInstallment,
                UserId = cashFlowInstallments.CashFlow_CashFlowId.UserId,
                ValueInstallments = cashFlowInstallments.ValueInstallment,
                Active = cashFlowInstallments.Active
            };
        }

        public static List<ProcessManagementModel.CashFlowInstallments> GetCashInputs(string date1 = null, string date2 = null, int? userId = null, int? accessGroupId = null, string userIdForm = null, string description = null, int? typeId = null)
        {
            var rs = new List<ProcessManagementModel.CashFlowInstallments>();
            var cashFlowList = new List<ProcessManagementModel.CashFlow>();

            var paramDate1 = DateTime.ParseExact(date1, "dd/MM/yyyy", new CultureInfo("en-US"));
            var paramDate2 = DateTime.ParseExact(date2, "dd/MM/yyyy", new CultureInfo("en-US"));

            if (!string.IsNullOrEmpty(date1) && !string.IsNullOrEmpty(date1))
            {
                if (accessGroupId.HasValue)
                {
                    var permissionsIds = User.GetAccessGroupPermissionsIds(accessGroupId.Value);

                    if (permissionsIds.Any())
                        cashFlowList = Cache.GetCashFlow().Where(c => c.CashFlowInstallments_CashFlowId.Any(x => x.PaymentDate >= paramDate1 && x.PaymentDate <= paramDate2) && c.IsInput && permissionsIds.Contains(c.Users.AccessGroup.Id) && c.Active).ToList();
                    else if (userId.HasValue)
                        cashFlowList = Cache.GetCashFlow().Where(c => c.CashFlowInstallments_CashFlowId.Any(x => x.PaymentDate >= paramDate1 && x.PaymentDate <= paramDate2) && c.IsInput && c.UserId.Equals(userId.Value) && c.Active).ToList();
                }
                else
                {
                    cashFlowList = Cache.GetCashFlow().Where(c => c.CashFlowInstallments_CashFlowId.Any(x => x.PaymentDate >= paramDate1 && x.PaymentDate <= paramDate2) && c.IsInput && c.Active).ToList();
                }
            }
            else
            {
                cashFlowList = Cache.GetCashFlow().Where(c => c.IsInput && c.Active).ToList();
            }

            if (!string.IsNullOrEmpty(userIdForm) && !userIdForm.Equals("0"))
                cashFlowList = cashFlowList.Where(c => c.UserId.Equals(Convert.ToInt32(userIdForm))).ToList();

            if (!string.IsNullOrEmpty(description))
                cashFlowList = cashFlowList.Where(c => c.Description.ToLower().Contains(description.ToLower())).ToList();

            if (typeId.HasValue)
                cashFlowList = cashFlowList.Where(c => c.TypeId.Equals(typeId.Value)).ToList();

            cashFlowList.ForEach(x => rs.AddRange(x.CashFlowInstallments_CashFlowId.Where(cc => cc.PaymentDate >= paramDate1 && cc.PaymentDate <= paramDate2 && cc.Active).ToList()));

            return rs.OrderBy(x => x.PaymentDate).ToList();
        }

        public static ProcessManagementModel.CashFlow GetById(int id)
        {
            var rs = Cache.GetCashFlow().Where(c => c.Id.Equals(id)).FirstOrDefault();
            rs.TotalAmount = Decimal.Parse(string.Format("{0:0.00}", rs.TotalAmount));

            return rs;
        }

        public static ProcessManagementModel.CashFlow GetParentByChildrenId(int id)
        {
            return Cache.GetCashFlow().Where(c => c.CashFlowInstallments_CashFlowId.Any(ci => ci.Id.Equals(id))).FirstOrDefault();
        }

        public static ProcessManagementModel.CashFlowInstallments GetInstallmentById(int id)
        {
            var rs = Cache.GetCashFlowInstallments().Where(c => c.Id.Equals(id)).FirstOrDefault();
            rs.ValueInstallment = Decimal.Parse(string.Format("{0:0.00}", rs.ValueInstallment));

            return rs;
        }

        public static List<ProcessManagementModel.Log> GetLogByCashFlowInstallmentId(int id)
        {
            var rs = new List<ProcessManagementModel.Log>();

            var cashFlow = GetParentByChildrenId(id);
            if (Cache.GetLog().Any(l => l.ColumnTable.Table.Name.Equals("CashFlow") && l.RegisterId.Equals(cashFlow.Id) && l.ColumnTable.Name.Equals("UserId")))
                foreach (var log in Cache.GetLog().Where(l => l.ColumnTable.Table.Name.Equals("CashFlow") && l.RegisterId.Equals(cashFlow.Id) && l.ColumnTable.Name.Equals("UserId")).ToList())
                {
                    log.Before = Core.User.GetUserById(Convert.ToInt32(log.Before)).UserName;
                    log.After = Core.User.GetUserById(Convert.ToInt32(log.After)).UserName;

                    rs.Add(log);
                }

            rs.AddRange(Cache.GetLog().Where(l => l.ColumnTable.Table.Name.Equals("CashFlowInstallments") && l.RegisterId.Equals(id)).ToList());
            return rs;
        }

        public static List<ProcessManagementModel.CashFlowInstallments> GetCashOutputs(string date1 = null, string date2 = null, int? userId = null, int? accessGroupId = null, string userIdForm = null, string description = null, int? typeId = null)
        {
            var rs = new List<ProcessManagementModel.CashFlowInstallments>();
            var cashFlowList = new List<ProcessManagementModel.CashFlow>();

            var paramDate1 = DateTime.ParseExact(date1, "dd/MM/yyyy", new CultureInfo("en-US"));
            var paramDate2 = DateTime.ParseExact(date2, "dd/MM/yyyy", new CultureInfo("en-US"));

            if (!string.IsNullOrEmpty(date1) && !string.IsNullOrEmpty(date1))
            {
                if (accessGroupId.HasValue)
                {
                    var permissionsIds = User.GetAccessGroupPermissionsIds(accessGroupId.Value);

                    if (permissionsIds.Any())
                        cashFlowList = Cache.GetCashFlow().Where(c => c.CashFlowInstallments_CashFlowId.Any(x => x.PaymentDate >= paramDate1 && x.PaymentDate <= paramDate2) && !c.IsInput && permissionsIds.Contains(c.Users.AccessGroup.Id) && c.Active).ToList();
                    else if (userId.HasValue)
                        cashFlowList = Cache.GetCashFlow().Where(c => c.CashFlowInstallments_CashFlowId.Any(x => x.PaymentDate >= paramDate1 && x.PaymentDate <= paramDate2) && !c.IsInput && c.UserId.Equals(userId.Value) && c.Active).ToList();
                }
                else
                {
                    cashFlowList = Cache.GetCashFlow().Where(c => c.CashFlowInstallments_CashFlowId.Any(x => x.PaymentDate >= paramDate1 && x.PaymentDate <= paramDate2) && !c.IsInput && c.Active).ToList();
                }
            }
            else
            {
                cashFlowList = Cache.GetCashFlow().Where(c => !c.IsInput && c.Active).ToList();
            }

            if (!string.IsNullOrEmpty(userIdForm) && !userIdForm.Equals("0"))
                cashFlowList = cashFlowList.Where(c => c.UserId.Equals(Convert.ToInt32(userIdForm))).ToList();

            if (!string.IsNullOrEmpty(description))
                cashFlowList = cashFlowList.Where(c => c.Description.ToLower().Contains(description.ToLower())).ToList();

            if (typeId.HasValue)
                cashFlowList = cashFlowList.Where(c => c.TypeId.Equals(typeId.Value)).ToList();

            cashFlowList.ForEach(x => rs.AddRange(x.CashFlowInstallments_CashFlowId.Where(cc => cc.PaymentDate >= paramDate1 && cc.PaymentDate <= paramDate2 && cc.Active).ToList()));

            return rs.OrderBy(x => x.PaymentDate).ToList();
        }

        public static DTO.ModelsView.CashFlowType CashFlowTypeToModelView(ProcessManagementModel.CashFlowType cashFlowType)
        {
            return new DTO.ModelsView.CashFlowType()
            {
                Id = cashFlowType.Id,
                Name = cashFlowType.Name,
                Details = cashFlowType.Details,
                Active = cashFlowType.Active
            };
        }

        public static List<ProcessManagementModel.CashFlowType> GetCashFlowType(bool isInput = true, bool all = false)
        {
            if (all)
                return Cache.GetCashFlowType();
            else if (isInput)
                return Cache.GetCashFlowType().Where(c => c.TypeId == 1 || c.TypeId == 2).ToList();
            else
                return Cache.GetCashFlowType().Where(c => c.TypeId == 1 || c.TypeId == 3).ToList();
        }

        public static ProcessManagementModel.CashFlowType GetCashFlowTypeById(int id)
        {
            return Cache.GetCashFlowType().FirstOrDefault(c => c.Id.Equals(id));
        }

        public static TransactionRS CreateCashFlowType(DTO.ModelsView.CashFlowType model, int userAuthenticatedId)
        {
            var rs = new TransactionRS();

            try
            {
                using (var context = new ProcessManagementEntities())
                {
                    if (model.Id.Equals(0))
                    {
                        context.CashFlowType.Add(new CashFlowType()
                        {
                            Name = model.Name,
                            Details = model.Details,
                            TypeId = model.TypeId,
                            Active = true
                        });

                        context.SaveChanges();
                    }
                    else
                    {
                        var column = new ColumnTable();
                        var table = Log.GetTableByName("CashFlowType");
                        var cashFlowTypeUpdated = context.CashFlowType.Find(model.Id);

                        if (cashFlowTypeUpdated.TypeId != model.TypeId)
                        {
                            column = table.ColumnTable.Where(c => c.Name.Equals("TypeId")).FirstOrDefault();
                            Log.Create(column, cashFlowTypeUpdated.TypeId.ToString(), model.TypeId.ToString(), userAuthenticatedId, model.Id, context);
                            cashFlowTypeUpdated.TypeId = model.TypeId;
                        }

                        if (cashFlowTypeUpdated.Name != model.Name)
                        {
                            column = table.ColumnTable.Where(c => c.Name.Equals("Name")).FirstOrDefault();
                            Log.Create(column, cashFlowTypeUpdated.Name, model.Name, userAuthenticatedId, model.Id, context);
                            cashFlowTypeUpdated.Name = model.Name;
                        }

                        if (cashFlowTypeUpdated.Details != model.Details)
                        {
                            column = table.ColumnTable.Where(c => c.Name.Equals("Details")).FirstOrDefault();
                            Log.Create(column, cashFlowTypeUpdated.Details, model.Details, userAuthenticatedId, model.Id, context);
                            cashFlowTypeUpdated.Details = model.Details;
                        }

                        context.Entry(cashFlowTypeUpdated).State = EntityState.Modified;
                        context.SaveChanges();
                    }

                    Core.Cache.ClearCache(new List<string>() { "CashFlowType" });
                }
            }
            catch (Exception ex)
            {
                rs.Successful = false;
                rs.ErrorsDetails.Add(new ErrorDetails()
                {
                    Message = SystemSettings.RegexReplaceString(ex.Message),
                    Source = $"{Source}.CreateCashFlowType",
                    DateTime = DateTime.Now
                });
            }

            return rs;
        }

        public static TransactionRS DeleteCashFlowTypeById(int id, int userId)
        {
            var rs = new TransactionRS();

            try
            {
                using (var context = new ProcessManagementEntities())
                {
                    var table = Log.GetTableByName("CashFlowType");
                    var column = table.ColumnTable.Where(c => c.Name.Equals("Active")).FirstOrDefault();

                    var cashFlowTypeType = context.CashFlowType.Find(id);
                    cashFlowTypeType.Active = false;
                    Log.Create(column, "1", "0", userId, id, context);

                    context.Entry(cashFlowTypeType).State = EntityState.Modified;
                    context.SaveChanges();
                }

                Core.Cache.ClearCache(new List<string>() { "CashFlowType" });
            }
            catch (Exception ex)
            {
                rs.Successful = false;
                rs.ErrorsDetails.Add(new ErrorDetails()
                {
                    Message = SystemSettings.RegexReplaceString(ex.Message),
                    Source = $"{Source}.DeleteCashFlowTypeById",
                    DateTime = DateTime.Now
                });
            }

            return rs;
        }

        public static List<ProcessManagementModel.CashFlowInstallments> GetAllByYear(int year, int? userId = null, int? accessGroupId = null)
        {
            var rs = new List<ProcessManagementModel.CashFlowInstallments>();
            var cashFlowList = new List<ProcessManagementModel.CashFlow>();

            if (accessGroupId.HasValue)
            {
                var permissionsIds = User.GetAccessGroupPermissionsIds(accessGroupId.Value);

                if (permissionsIds.Any())
                    cashFlowList = Cache.GetCashFlow().Where(c => c.CashFlowInstallments_CashFlowId.Any(x => x.PaymentDate.Year.Equals(year)) && permissionsIds.Contains(c.Users.AccessGroup.Id) && c.Active).ToList();
                else if (userId.HasValue)
                    cashFlowList = Cache.GetCashFlow().Where(c => c.CashFlowInstallments_CashFlowId.Any(x => x.PaymentDate.Year.Equals(year)) && c.UserId.Equals(userId.Value) && c.Active).ToList();
            }
            else
            {
                cashFlowList = Cache.GetCashFlow().Where(c => c.CashFlowInstallments_CashFlowId.Any(x => x.PaymentDate.Year.Equals(year)) && c.Active).ToList();
            }

            cashFlowList.ForEach(x => rs.AddRange(x.CashFlowInstallments_CashFlowId.Where(cc => cc.PaymentDate.Year.Equals(year) && cc.Active).ToList()));

            return rs;
        }

        public static TransactionRS Save(ProcessManagement.DTO.ModelsView.CashFlow model, List<ProcessManagementModel.Images> images, int userAuthenticatedId)
        {
            var rs = new TransactionRS();

            try
            {
                if (model.UserId.Equals(0))
                    model.UserId = userAuthenticatedId;

                using (var context = new ProcessManagementEntities())
                {
                    if (!model.Id.Equals(0))
                    {
                        var column = new ColumnTable();
                        var table = Log.GetTableByName("CashFlowInstallments");
                        var cashFlowInstallmentUpdated = context.CashFlowInstallments.Where(c => c.Id.Equals(model.Id)).FirstOrDefault();

                        if (cashFlowInstallmentUpdated.Description != model.Description)
                        {
                            column = table.ColumnTable.Where(c => c.Name.Equals("Description")).FirstOrDefault();
                            Log.Create(column, cashFlowInstallmentUpdated.Description, model.Description, userAuthenticatedId, model.Id, context);
                            cashFlowInstallmentUpdated.Description = model.Description;
                        }

                        if (cashFlowInstallmentUpdated.ValueInstallment != model.TotalAmount)
                        {
                            column = table.ColumnTable.Where(c => c.Name.Equals("ValueInstallment")).FirstOrDefault();
                            Log.Create(column, cashFlowInstallmentUpdated.ValueInstallment.ToString("#.##"), string.Format("{0:0.00}", model.TotalAmount.ToString()), userAuthenticatedId, model.Id, context);
                            cashFlowInstallmentUpdated.ValueInstallment = model.TotalAmount;
                        }

                        if (cashFlowInstallmentUpdated.Details != model.Details)
                        {
                            column = table.ColumnTable.Where(c => c.Name.Equals("Details")).FirstOrDefault();
                            Log.Create(column, cashFlowInstallmentUpdated.Details, model.Details, userAuthenticatedId, model.Id, context);
                            cashFlowInstallmentUpdated.Details = model.Details;
                        }

                        if (cashFlowInstallmentUpdated.PaymentDate != model.FirstPaymentDate)
                        {
                            column = table.ColumnTable.Where(c => c.Name.Equals("PaymentDate")).FirstOrDefault();
                            Log.Create(column, cashFlowInstallmentUpdated.PaymentDate.ToString("dd/MM/yyyy"), model.FirstPaymentDate.ToString("dd/MM/yyyy"), userAuthenticatedId, model.Id, context);
                            cashFlowInstallmentUpdated.PaymentDate = model.FirstPaymentDate;
                        }

                        cashFlowInstallmentUpdated.Active = true;
                        context.Entry(cashFlowInstallmentUpdated).State = System.Data.Entity.EntityState.Modified;

                        var cashFlowUpdated = context.CashFlow.Where(c => c.Id.Equals(cashFlowInstallmentUpdated.CashFlowId)).FirstOrDefault();
                        cashFlowUpdated.Installments = cashFlowUpdated.CashFlowInstallments_CashFlowId.Count();
                        cashFlowUpdated.TotalAmount = cashFlowUpdated.CashFlowInstallments_CashFlowId.Select(x => x.ValueInstallment).Sum();

                        table = Log.GetTableByName("CashFlow");
                        if (cashFlowUpdated.TypeId != model.TypeId)
                        {
                            column = table.ColumnTable.Where(c => c.Name.Equals("TypeId")).FirstOrDefault();
                            Log.Create(column, cashFlowUpdated.TypeId.ToString(), model.TypeId.ToString(), userAuthenticatedId, model.Id, context);
                            cashFlowUpdated.TypeId = model.TypeId;
                        }

                        context.Entry(cashFlowUpdated).State = System.Data.Entity.EntityState.Modified;

                        foreach (var image in images)
                        {
                            image.Id = context.Images.Any(i => i.Content.Equals(image.Content)) ? context.Images.Where(i => i.Content.Equals(image.Content)).FirstOrDefault().Id : context.Images.Add(image).Id;
                            if (!context.CashFlowImages.Any(c => c.CashFlowInstallmentsId.Equals(cashFlowInstallmentUpdated.Id) && c.ImageId.Equals(image.Id)))
                                context.CashFlowImages.Add(new CashFlowImages()
                                {
                                    CashFlowInstallmentsId = cashFlowInstallmentUpdated.Id,
                                    ImageId = image.Id
                                });

                            context.SaveChanges();
                        }

                        var imageIds = images.Select(i => i.Id).ToList();
                        context.CashFlowImages.RemoveRange(context.CashFlowImages.Where(c => c.CashFlowInstallmentsId.Equals(model.Id) && !imageIds.Contains(c.ImageId)));

                        context.SaveChanges();
                        Core.Cache.ClearCache(new List<string>() { "CashFlow", "CashFlowInstallments", "Log", "Images" });
                    }
                    else
                    {
                        model.Active = true;
                        var cashFlowInstallments = new List<ProcessManagementModel.CashFlowInstallments>();

                        for (var i = 1; i <= Convert.ToInt32(model.Installments); i++)
                        {
                            cashFlowInstallments.Add(new ProcessManagementModel.CashFlowInstallments()
                            {
                                Description = model.Description,
                                Details = model.Details,
                                Installment = $"{i} de {model.Installments}",
                                PaymentDate = i.Equals(1) ? model.FirstPaymentDate : model.FirstPaymentDate.AddMonths((i - 1)),
                                ValueInstallment = model.TotalAmount / model.Installments,
                                Active = true
                            });
                        }

                        var cashFlow = context.CashFlow.Add(new ProcessManagementModel.CashFlow()
                        {
                            Active = model.Active,
                            DateCreation = DateTime.Now,
                            Description = model.Description,
                            Details = model.Details,
                            IsInput = model.IsInput,
                            TotalAmount = model.TotalAmount,
                            UserId = model.UserId,
                            CashFlowInstallments_CashFlowId = cashFlowInstallments,
                            FirstPaymentDate = model.FirstPaymentDate,
                            Installments = model.Installments,
                            TypeId = model.TypeId
                        });

                        foreach (var installment in cashFlow.CashFlowInstallments_CashFlowId)
                        {
                            foreach (var image in images)
                            {
                                var imageId = context.Images.Any(img => img.Content.Equals(image.Content)) ? context.Images.Where(img => img.Content.Equals(image.Content)).FirstOrDefault().Id : context.Images.Add(image).Id;
                                context.CashFlowImages.Add(new CashFlowImages()
                                {
                                    ImageId = imageId,
                                    CashFlowInstallments = installment
                                });

                               context.SaveChanges();
                            }
                        }

                        context.SaveChanges();
                        if (!model.UserId.Equals(userAuthenticatedId))
                            SaveLogAlterUser(userAuthenticatedId, model.UserId, cashFlow.DateCreation);

                        Core.Cache.ClearCache(new List<string>() { "CashFlow", "CashFlowInstallments", "Log", "Images" });
                    }
                }
            }
            catch (Exception ex)
            {
                rs.Successful = false;
                rs.ErrorsDetails.Add(new ErrorDetails()
                {
                    Message = SystemSettings.RegexReplaceString(ex.Message),
                    Source = $"{Source}.Save",
                    DateTime = DateTime.Now
                });
            }

            return rs;
        }

        public static List<ProcessManagementModel.Type> GetTypes()
        {
            return Cache.GetTypes();
        }

        private static void SaveLogAlterUser(int userIdBefore, int userIdAfter, DateTime dateCreation)
        {
            using (var context = new ProcessManagementEntities())
            {
                if (context.CashFlow.Any(c => c.UserId.Equals(userIdAfter) && (c.DateCreation.Year.Equals(dateCreation.Year) && c.DateCreation.Month.Equals(dateCreation.Month) && c.DateCreation.Day.Equals(dateCreation.Day) 
                && c.DateCreation.Hour.Equals(dateCreation.Hour) && c.DateCreation.Minute.Equals(dateCreation.Minute) && c.DateCreation.Second.Equals(dateCreation.Second))))
                {
                    var registerId = context.CashFlow.Where(c => c.UserId.Equals(userIdAfter) && (c.DateCreation.Year.Equals(dateCreation.Year) && c.DateCreation.Month.Equals(dateCreation.Month) && c.DateCreation.Day.Equals(dateCreation.Day)
                    && c.DateCreation.Hour.Equals(dateCreation.Hour) && c.DateCreation.Minute.Equals(dateCreation.Minute) && c.DateCreation.Second.Equals(dateCreation.Second))).FirstOrDefault().Id;

                    var table = Log.GetTableByName("CashFlow");
                    var column = table.ColumnTable.Where(c => c.Name.Equals("UserId")).FirstOrDefault();
                    Log.Create(column, userIdBefore.ToString(), userIdAfter.ToString(), userIdBefore, registerId, context);

                    context.SaveChanges();
                }
            }
        }

        public static TransactionRS Delete(int id)
        {
            var rs = new TransactionRS();

            try
            {
                using (var context = new ProcessManagementEntities())
                {
                    var cashFlowInstallmentsDeleted = context.CashFlowInstallments.Where(c => c.Id.Equals(id)).FirstOrDefault();
                    cashFlowInstallmentsDeleted.Active = false;
                    context.Entry(cashFlowInstallmentsDeleted).State = EntityState.Modified;

                    context.SaveChanges();
                    Core.Cache.ClearCache(new List<string>() { "CashFlow", "CashFlowInstallments" });
                }
            }
            catch (Exception ex)
            {
                rs.Successful = false;
                rs.ErrorsDetails.Add(new ErrorDetails()
                {
                    Message = SystemSettings.RegexReplaceString(ex.Message),
                    Source = $"{Source}.Delete",
                    DateTime = DateTime.Now
                });
            }

            return rs;
        }

        public static List<ProcessManagementModel.Images> GetAttachments(int id)
        {
            return Cache.GetCashFlowInstallments().Where(c => c.Id.Equals(id)).FirstOrDefault().CashFlowImages.Select(x => x.Images).ToList();
        }
    }
}
