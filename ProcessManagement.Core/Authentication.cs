﻿using ProcessManagement.DTO.Envelopes;
using ProcessManagement.DTO.Envelopes.Base;
using ProcessManagementModel;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Data.Entity;

namespace ProcessManagement.Core
{
    public class Authentication
    {
        private static string Source = "ProcessManagement.Core.Authentication";

        public static SignInRS SignIn(SignInRQ rq)
        {
            var rs = new SignInRS();

            try
            {
                using (var context = new ProcessManagementEntities())
                {
                    if (!context.Users.Include(c => c.AccessGroup).Any(u => u.UserName.Equals(rq.UserName) && u.Password.Equals(rq.Password)))
                    {
                        rs.Successful = false;
                        rs.User = null;
                        rs.ErrorsDetails.Add(new ErrorDetails()
                        {
                            Message = "Usuario ou senha invalidos. Preencha os campos corretamente.",
                            Source = $"{Source}.SignIn",
                            DateTime = DateTime.Now
                        });
                    }
                    else
                    {
                        rs.User = context.Users.Where(u => u.UserName.Equals(rq.UserName) && u.Password.Equals(rq.Password)).FirstOrDefault();
                    }
                }
            }
            catch (Exception ex)
            {
                rs.Successful = false;
                rs.User = null;
                rs.ErrorsDetails.Add(new ErrorDetails()
                {
                    Message = SystemSettings.RegexReplaceString(ex.Message),
                    Source = $"{Source}.SignIn",
                    DateTime = DateTime.Now
                });
            }

            return rs;
        }

        private static string GenerateToken()
        {
            var table = new DataTable();
            string expression = string.Format(SystemSettings.RuleToToken, DateTime.Now.ToString("yyyy-MM-dd").Replace("-","")); ;

            return table.Compute(expression, null).ToString();
        }
    }
}
