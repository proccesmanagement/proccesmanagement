﻿using ProcessManagementModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Caching;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;

namespace ProcessManagement.Core
{
    public class Cache
    {
        private static ObjectCache _cache = MemoryCache.Default;

        public static void ClearCache(List<string> cachesName)
        {
            foreach (var cacheName in cachesName)
                if (_cache.Get(cacheName) != null)
                    _cache.Remove(cacheName);
        }

        public static List<ProcessManagementModel.CashFlow> GetCashFlow()
        {
            try
            {
                var cacheName = "CashFlow";

                if (_cache.Get(cacheName) != null)
                    return (List<ProcessManagementModel.CashFlow>)_cache.Get(cacheName);

                using (var context = new ProcessManagementEntities())
                {
                    var cashFlows = context.CashFlow.Include(c => c.Users).Include(c => c.Users.AccessGroup).Include(c => c.CashFlowInstallments_CashFlowId).Include(c => c.CashFlowInstallments_CashFlowId.Select(x => x.Status)).ToList();

                    _cache.Add(cacheName, cashFlows, new CacheItemPolicy { AbsoluteExpiration = DateTimeOffset.Now.AddHours(1) });
                    return cashFlows;
                }
            }
            catch (Exception e)
            {
                throw new Exception("Erro ao recuperar cashFlows : " + e.Message + (e.InnerException != null ? e.InnerException.Message : ""));
            }
        }

        public static List<ProcessManagementModel.CashFlowInstallments> GetCashFlowInstallments()
        {
            try
            {
                var cacheName = "CashFlowInstallments";

                if (_cache.Get(cacheName) != null)
                    return (List<ProcessManagementModel.CashFlowInstallments>)_cache.Get(cacheName);

                using (var context = new ProcessManagementEntities())
                {
                    var cashFlowInstallments = context.CashFlowInstallments
                                                      .Include(c => c.CashFlow_CashFlowId)
                                                      .Include(c => c.CashFlow_CashFlowId.Users)
                                                      .Include(c => c.CashFlow_CashFlowId.CashFlowType)
                                                      .Include(c => c.CashFlowImages)
                                                      .Include(c => c.CashFlowImages.Select(x => x.Images)).ToList();

                    _cache.Add(cacheName, cashFlowInstallments, new CacheItemPolicy { AbsoluteExpiration = DateTimeOffset.Now.AddHours(1) });
                    return cashFlowInstallments;
                }
            }
            catch (Exception e)
            {
                throw new Exception("Erro ao recuperar cashFlowInstallments : " + e.Message + (e.InnerException != null ? e.InnerException.Message : ""));
            }
        }

        public static List<ProcessManagementModel.CashFlowType> GetCashFlowType()
        {
            try
            {
                var cacheName = "CashFlowType";

                if (_cache.Get(cacheName) != null)
                    return (List<ProcessManagementModel.CashFlowType>)_cache.Get(cacheName);

                using (var context = new ProcessManagementEntities())
                {
                    var cashFlowType = context.CashFlowType.Include(c => c.Type).Where(c => c.Active).ToList();

                    _cache.Add(cacheName, cashFlowType, new CacheItemPolicy { AbsoluteExpiration = DateTimeOffset.Now.AddHours(1) });
                    return cashFlowType;
                }
            }
            catch (Exception e)
            {
                throw new Exception("Erro ao recuperar cashFlowType : " + e.Message + (e.InnerException != null ? e.InnerException.Message : ""));
            }
        }

        public static List<ProcessManagementModel.Log> GetLog()
        {
            try
            {
                var cacheName = "Log";

                if (_cache.Get(cacheName) != null)
                    return (List<ProcessManagementModel.Log>)_cache.Get(cacheName);

                using (var context = new ProcessManagementEntities())
                {
                    var logs = context.Log.Include(l => l.Users).Include(l => l.ColumnTable.Table).ToList();

                    _cache.Add(cacheName, logs, new CacheItemPolicy { AbsoluteExpiration = DateTimeOffset.Now.AddHours(1) });
                    return logs;
                }
            }
            catch (Exception e)
            {
                throw new Exception("Erro ao recuperar logs : " + e.Message + (e.InnerException != null ? e.InnerException.Message : ""));
            }
        }

        public static List<ProcessManagementModel.Images> GetImages()
        {
            try
            {
                var cacheName = "Images";

                if (_cache.Get(cacheName) != null)
                    return (List<ProcessManagementModel.Images>)_cache.Get(cacheName);

                using (var context = new ProcessManagementEntities())
                {
                    var images = context.Images.ToList();

                    _cache.Add(cacheName, images, new CacheItemPolicy { AbsoluteExpiration = DateTimeOffset.Now.AddHours(1) });
                    return images;
                }
            }
            catch (Exception e)
            {
                throw new Exception("Erro ao recuperar images : " + e.Message + (e.InnerException != null ? e.InnerException.Message : ""));
            }
        }

        public static List<ProcessManagementModel.Users> GetUsers()
        {
            try
            {
                var cacheName = "Users";

                if (_cache.Get(cacheName) != null)
                    return (List<ProcessManagementModel.Users>)_cache.Get(cacheName);

                using (var context = new ProcessManagementEntities())
                {
                    var users = context.Users.Include(x => x.AccessGroup).Include(x => x.Images).Include(x => x.AccessGroup.Clients).ToList();

                    _cache.Add(cacheName, users, new CacheItemPolicy { AbsoluteExpiration = DateTimeOffset.Now.AddHours(1) });
                    return users;
                }
            }
            catch (Exception e)
            {
                throw new Exception("Erro ao recuperar users : " + e.Message + (e.InnerException != null ? e.InnerException.Message : ""));
            }
        }

        public static List<ProcessManagementModel.AccessGroup> GetAccessGroup()
        {
            try
            {
                var cacheName = "AccessGroup";

                if (_cache.Get(cacheName) != null)
                    return (List<ProcessManagementModel.AccessGroup>)_cache.Get(cacheName);

                using (var context = new ProcessManagementEntities())
                {
                    var accessGroup = context.AccessGroup.ToList();

                    _cache.Add(cacheName, accessGroup, new CacheItemPolicy { AbsoluteExpiration = DateTimeOffset.Now.AddHours(1) });
                    return accessGroup;
                }
            }
            catch (Exception e)
            {
                throw new Exception("Erro ao recuperar accessGroup : " + e.Message + (e.InnerException != null ? e.InnerException.Message : ""));
            }
        }

        public static List<ProcessManagementModel.AccessGroupPermissions> GetAccessGroupPermissions()
        {
            try
            {
                var cacheName = "AccessGroupPermissions";

                if (_cache.Get(cacheName) != null)
                    return (List<ProcessManagementModel.AccessGroupPermissions>)_cache.Get(cacheName);

                using (var context = new ProcessManagementEntities())
                {
                    var accessGroupPermissions = context.AccessGroupPermissions.Include(a => a.AccessGroup_AccessGroupId).ToList();

                    _cache.Add(cacheName, accessGroupPermissions, new CacheItemPolicy { AbsoluteExpiration = DateTimeOffset.Now.AddHours(1) });
                    return accessGroupPermissions;
                }
            }
            catch (Exception e)
            {
                throw new Exception("Erro ao recuperar accessGroupPermissions : " + e.Message + (e.InnerException != null ? e.InnerException.Message : ""));
            }
        }

        public static List<ProcessManagementModel.AccessGroupMenuPermissions> GetAccessGroupMenuPermissions()
        {
            try
            {
                var cacheName = "AccessGroupMenuPermissions";

                if (_cache.Get(cacheName) != null)
                    return (List<ProcessManagementModel.AccessGroupMenuPermissions>)_cache.Get(cacheName);

                using (var context = new ProcessManagementEntities())
                {
                    var accessGroupMenuPermissions = context.AccessGroupMenuPermissions.Include(m => m.Menu).ToList();

                    _cache.Add(cacheName, accessGroupMenuPermissions, new CacheItemPolicy { AbsoluteExpiration = DateTimeOffset.Now.AddHours(1) });
                    return accessGroupMenuPermissions;
                }
            }
            catch (Exception e)
            {
                throw new Exception("Erro ao recuperar accessGroupMenuPermissions : " + e.Message + (e.InnerException != null ? e.InnerException.Message : ""));
            }
        }

        public static List<ProcessManagementModel.Type> GetTypes()
        {
            try
            {
                var cacheName = "Type";

                if (_cache.Get(cacheName) != null)
                    return (List<ProcessManagementModel.Type>)_cache.Get(cacheName);

                using (var context = new ProcessManagementEntities())
                {
                    var type = context.Type.ToList();

                    _cache.Add(cacheName, type, new CacheItemPolicy { AbsoluteExpiration = DateTimeOffset.Now.AddHours(1) });
                    return type;
                }
            }
            catch (Exception e)
            {
                throw new Exception("Erro ao recuperar type : " + e.Message + (e.InnerException != null ? e.InnerException.Message : ""));
            }
        }
    }
}
