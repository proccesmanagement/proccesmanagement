﻿using ProcessManagementModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;

namespace ProcessManagement.Core
{
    public class Menu
    {
        public static Dictionary<int, List<DTO.CacheModels.Menu>> Load(int? accessGroupId = null)
        {
            var menu = new Dictionary<int, List<DTO.CacheModels.Menu>>();
            var menuItems = new List<ProcessManagementModel.Menu>();

            using (var context = new ProcessManagementEntities())
            {
                menuItems = accessGroupId.HasValue ? context.AccessGroupMenuPermissions.Include(x => x.Menu).Include(x => x.Menu.Menu_ParentId1).Where(a => a.AccessGroupId.Equals(accessGroupId.Value)).Select(x => x.Menu).ToList() :
                                context.Menu.Include(x => x.Menu_ParentId1).Where(m => m.Active && m.ParentId == null).ToList();
            }

            if (!accessGroupId.HasValue)
                accessGroupId = 0;

            menu[accessGroupId.Value] = new List<DTO.CacheModels.Menu>();

            foreach (var menuItem in menuItems)
            {
                if (menuItem.ParentId == null)
                {
                    menu[accessGroupId.Value].Add(new DTO.CacheModels.Menu()
                    {
                        Id = menuItem.Id,
                        Text = menuItem.Text,
                        Icon = menuItem.Icon,
                        Menu_Childs = GetMenuChilds(menuItem),
                        View = GetView(menuItem),
                        OrdemItem = menuItem.OrderItem
                    });
                }
                else if (!menu[accessGroupId.Value].Any(x => x.Id.Equals(menuItem.ParentId.Value)))
                {
                    var parentMenu = GetMenuById(menuItem.ParentId.Value);

                    menu[accessGroupId.Value].Add(new DTO.CacheModels.Menu()
                    {
                        Id = parentMenu.Id,
                        Text = parentMenu.Text,
                        Icon = parentMenu.Icon,
                        Menu_Childs = GetMenuChilds(parentMenu, menuItems.Select(x => x.Id).ToList()),
                        View = GetView(menuItem),
                        OrdemItem = parentMenu.OrderItem
                    });
                }
            }

            menu[accessGroupId.Value] = menu[accessGroupId.Value].OrderBy(x => x.OrdemItem).ToList();

            return menu;
        }

        private static List<DTO.CacheModels.Menu> GetMenuChilds(ProcessManagementModel.Menu menu, List<int> ids = null)
        {
            var menuChilds = new List<DTO.CacheModels.Menu>();
            var menuChildsModel = new List<ProcessManagementModel.Menu>();

            using (var context = new ProcessManagementEntities())
            {
                if (ids != null && ids.Any())
                    menuChildsModel = context.Menu.Where(m => m.ParentId.HasValue && m.ParentId.Value.Equals(menu.Id) && ids.Contains(m.Id)).ToList();
                else
                    menuChildsModel = context.Menu.Where(m => m.ParentId.HasValue && m.ParentId.Value.Equals(menu.Id)).ToList();
            }

            foreach (var menuChildModel in menuChildsModel.OrderBy(m => m.OrderItem))
            {
                menuChilds.Add(new DTO.CacheModels.Menu()
                {
                    Text = menuChildModel.Text,
                    Icon = menuChildModel.Icon,
                    Id = menuChildModel.Id,
                    View = GetView(menuChildModel)
                });
            }

            return menuChilds;
        }

        private static DTO.CacheModels.Views GetView(ProcessManagementModel.Menu menu)
        {
            var view = new Views();

            if (menu.ViewId.HasValue)
            {
                using (var context = new ProcessManagementEntities())
                {
                    view = context.Views.Include(x => x.Controllers).Where(v => v.Id.Equals(menu.ViewId.Value)).FirstOrDefault();
                }

                return new DTO.CacheModels.Views()
                {
                    Active = view.Active,
                    Id = view.Id,
                    ViewName = view.ViewName,
                    Controller = GetController(view)
                };
            }

            return new DTO.CacheModels.Views();
        }

        private static DTO.CacheModels.Controllers GetController(ProcessManagementModel.Views view)
        {
            return new DTO.CacheModels.Controllers()
            {
                Id = view.Controllers.Id,
                Active = view.Controllers.Active,
                Controller = view.Controllers.Controller
            };
        }

        private static ProcessManagementModel.Menu GetMenuById(int id)
        {
            var menu = new ProcessManagementModel.Menu();

            using (var context = new ProcessManagementEntities())
            {
                menu = context.Menu.Find(id);
            }

            return menu;
        }
    }
}
