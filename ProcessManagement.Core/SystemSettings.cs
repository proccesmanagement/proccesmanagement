﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace ProcessManagement.Core
{
    public class SystemSettings
    {
        public static string ConnectionStringProcessManagement
        {
            get { return ConfigurationManager.ConnectionStrings["ProcessManagement"].ConnectionString; }
        }

        public static string RuleToToken
        {
            get { return ConfigurationManager.AppSettings["RuleToToken"]; }
        }

        public static string RuleFromToken
        {
            get { return ConfigurationManager.AppSettings["RuleFromToken"]; }
        }

        public static string RegexReplaceString (string value)
        {
            return Regex.Replace(value, "[^0-9a-zA-Z- ]+", "");
        }
    }
}
