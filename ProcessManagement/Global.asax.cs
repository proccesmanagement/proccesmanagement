﻿using ProcessManagementModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Principal;
using System.Web;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using System.Web.Security;

namespace ProcessManagement
{
    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
        }

        protected void Application_PostAuthenticateRequest(Object sender, EventArgs e)
        {
            if (FormsAuthentication.CookiesSupported == true)
            {
                if (Request.Cookies[FormsAuthentication.FormsCookieName] != null)
                {
                    try
                    {
                        var userId = FormsAuthentication.Decrypt(Request.Cookies[FormsAuthentication.FormsCookieName].Value).Name;
                        var role = string.Empty;

                        using (var context = new ProcessManagementEntities())
                        {
                            var user = context.Users.Where(u => u.Id.Equals(userId)).FirstOrDefault();
                            role = user.AccessGroup.Name;
                        }
                        
                        HttpContext.Current.User = new GenericPrincipal( new GenericIdentity(userId, "Forms"), role.Split(';'));
                    }
                    catch (Exception)
                    {
                        
                    }
                }
            }
        }
    }
}
