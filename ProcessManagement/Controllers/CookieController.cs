﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ProcessManagement.Controllers
{
    public class CookieController : Controller
    {
        public string GetCookie(string cookieName)
        {
            if (ControllerContext.HttpContext.Request.Cookies.AllKeys.Contains(cookieName))
                return ControllerContext.HttpContext.Request.Cookies[cookieName].Value;

            return string.Empty;
        }

        public void CreateCookie(string cookieName, string value)
        {
            ControllerContext.HttpContext.Response.Cookies.Add(new HttpCookie(cookieName, value));
        }

        public void RemoveCookie(string cookieName)
        {
            if (ControllerContext.HttpContext.Request.Cookies.AllKeys.Contains(cookieName))
            {
                var cookie = ControllerContext.HttpContext.Request.Cookies[cookieName];
                cookie.Expires = DateTime.Now.AddDays(-1);
                ControllerContext.HttpContext.Response.Cookies.Add(cookie);
            }
        }
    }
}