﻿using ProcessManagement.Cache;
using ProcessManagement.Core;
using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;


namespace ProcessManagement.Controllers
{
    [Authorize]
    public class CashFlowController : ApplicationController
    {
        public ActionResult CashInput()
        {
            var month = DateTime.Now.Month.ToString().Length.Equals(1) ? $"0{DateTime.Now.Month.ToString()}" : DateTime.Now.Month.ToString();

            var date1 = $"01/{month}/{DateTime.Now.Year}";
            var date2 = $"{DateTime.DaysInMonth(DateTime.Now.Year, DateTime.Now.Month)}/{month}/{DateTime.Now.Year}";

            ParseValuesToTemp(CashFlow.GetCashInputs(date1, date2, UserAuthenticated.Id, AccessGroupUser.Id));

            ViewBag.Date1 = date1;
            ViewBag.Date2 = date2;
            ViewBag.UserId = UnionSelectList(new SelectList(Core.User.GetUsers(UserAuthenticated.AccessGroup.ClientId, UserAuthenticated.AccessGroupId), "Id", "UserName"));
            ViewBag.TypeId = UnionSelectList(new SelectList(Core.CashFlow.GetCashFlowType(), "Id", "Name", 0));
            ViewBag.Approver = IsApprover;

            return View();
        }

        [HttpPost]
        public ActionResult CashInput(FormCollection form)
        {
            try
            {
                var date1 = form["date1"];
                var date2 = form["date2"];
                var group = form["groupOption"];
                var userId = form["userId"];
                var descriprion = form["description"];
                var typeId = form["TypeId"];

                int? typeIdSearch = null;
                if (typeId != null && typeId != "0")
                    typeIdSearch = Convert.ToInt32(typeId);

                ParseValuesToTemp(CashFlow.GetCashInputs(date1, date2, UserAuthenticated.Id, AccessGroupUser.Id, userId, descriprion, typeIdSearch), group);

                ViewBag.Date1 = date1;
                ViewBag.Date2 = date2;
                ViewBag.Description = string.IsNullOrEmpty(descriprion) ? null : descriprion;
                ViewBag.TypeId = UnionSelectList(new SelectList(Core.CashFlow.GetCashFlowType(), "Id", "Name", typeId));
                ViewBag.UserId = UnionSelectList(new SelectList(Core.User.GetUsers(UserAuthenticated.AccessGroup.ClientId, UserAuthenticated.AccessGroupId), "Id", "UserName", string.IsNullOrEmpty(userId) ? 0 : Convert.ToInt32(userId)));
                ViewBag.Approver = IsApprover;
            }
            catch (Exception ex)
            {
                OpenModal(StateModal.Danger, "Erro", ex.Message);
            }

            return View("CashInput");
        }

        public ActionResult CreateInput()
        {
            ViewBag.IsEdit = false;
            ViewBag.Authority = Core.User.AccessGroupIsAuthority(UserAuthenticated.AccessGroupId);
            ViewBag.UserId = new SelectList(Core.User.GetUsers(UserAuthenticated.AccessGroup.ClientId, UserAuthenticated.AccessGroupId), "Id", "UserName", UserAuthenticatedId);
            ViewBag.TypeId = new SelectList(Core.CashFlow.GetCashFlowType(), "Id", "Name");

            return View();
        }

        [HttpPost]
        public ActionResult Save(ProcessManagement.DTO.ModelsView.CashFlow cashFlow, bool input, string inputGuid)
        {
            cashFlow.IsInput = input;

            if (cashFlow.Id.Equals(0))
                ModelState.Remove("Id");

            if (ModelState.IsValid)
            {
                var images = !string.IsNullOrEmpty(inputGuid) ? GetImagesByGuid(inputGuid) : new List<ProcessManagementModel.Images>();

                var rs = CashFlow.Save(cashFlow, images, UserAuthenticatedId);

                if (rs.Successful)
                {
                    OpenModal(StateModal.Success, "Sucesso", "Movimentação salva com sucesso.");
                    return RedirectToAction(input ? "CashInput" : "CashOutput", "CashFlow");
                }
                else
                {
                    OpenModal(StateModal.Danger, "Erro", rs.ErrorsDetails.First().Message);
                    return RedirectToAction(input ? "CashInput" : "CashOutput", "CashFlow");
                }
            }

            OpenModal(StateModal.Warning, "Atenção", "Preencha todos os campos corretamente.");
            return View(input ? "CreateInput" : "CreateOutput", "CashFlow", cashFlow);
        }

        public ActionResult Edit(int id, bool input)
        {
            ViewBag.IsEdit = true;
            ViewBag.Authority = false;
            var model = CashFlow.GetInstallmentById(id);
            ViewBag.UserId = new SelectList(Core.User.GetUsers(UserAuthenticated.AccessGroup.ClientId, UserAuthenticated.AccessGroupId), "Id", "UserName", model.CashFlow_CashFlowId.UserId);
            ViewBag.TypeId = new SelectList(Core.CashFlow.GetCashFlowType(input), "Id", "Name");

            return View(input ? "CreateInput" : "CreateOutput", CashFlow.InstallmentsToCashFlowView(model));
        }

        public ActionResult CashOutput()
        {
            var month = DateTime.Now.Month.ToString().Length.Equals(1) ? $"0{DateTime.Now.Month.ToString()}" : DateTime.Now.Month.ToString();

            var date1 = $"01/{month}/{DateTime.Now.Year}";
            var date2 = $"{DateTime.DaysInMonth(DateTime.Now.Year, DateTime.Now.Month)}/{month}/{DateTime.Now.Year}";

            ParseValuesToTemp(CashFlow.GetCashOutputs(date1, date2, UserAuthenticated.Id, AccessGroupUser.Id));

            ViewBag.Date1 = date1;
            ViewBag.Date2 = date2;
            ViewBag.UserId = UnionSelectList(new SelectList(Core.User.GetUsers(UserAuthenticated.AccessGroup.ClientId, UserAuthenticated.AccessGroupId), "Id", "UserName"));
            ViewBag.TypeId = UnionSelectList(new SelectList(Core.CashFlow.GetCashFlowType(false), "Id", "Name", 0));
            ViewBag.Approver = IsApprover;

            return View();
        }

        [HttpPost]
        public ActionResult CashOutput(FormCollection form)
        {
            try
            {
                var date1 = form["date1"];
                var date2 = form["date2"];
                var group = form["groupOption"];
                var userId = form["userId"];
                var descriprion = form["description"];
                var typeId = form["TypeId"];

                int? typeIdSearch = null;
                if (typeId != null && typeId != "0")
                    typeIdSearch = Convert.ToInt32(typeId);

                ParseValuesToTemp(CashFlow.GetCashOutputs(date1, date2, UserAuthenticated.Id, AccessGroupUser.Id, userId, descriprion, typeIdSearch), group);

                ViewBag.Date1 = date1;
                ViewBag.Date2 = date2;
                ViewBag.Description = string.IsNullOrEmpty(descriprion) ? null : descriprion;
                ViewBag.TypeId = UnionSelectList(new SelectList(Core.CashFlow.GetCashFlowType(false), "Id", "Name", typeId));
                ViewBag.UserId = UnionSelectList(new SelectList(Core.User.GetUsers(UserAuthenticated.AccessGroup.ClientId, UserAuthenticated.AccessGroupId), "Id", "UserName", string.IsNullOrEmpty(userId) ? 0 : Convert.ToInt32(userId)));
                ViewBag.Approver = IsApprover;
            }
            catch (Exception ex)
            {
                OpenModal(StateModal.Danger, "Erro", ex.Message);
            }

            return View("CashOutput");
        }

        public ActionResult CreateOutput()
        {
            ViewBag.IsEdit = false;
            ViewBag.Authority = Core.User.AccessGroupIsAuthority(UserAuthenticated.AccessGroupId);
            ViewBag.UserId = new SelectList(Core.User.GetUsers(UserAuthenticated.AccessGroup.ClientId, UserAuthenticated.AccessGroupId), "Id", "UserName", UserAuthenticatedId);
            ViewBag.TypeId = new SelectList(Core.CashFlow.GetCashFlowType(false), "Id", "Name");

            return View();
        }

        public ActionResult Details(int id)
        {
            var model = new ProcessManagement.DTO.ModelsView.CashFlowInstallmentsDetails()
            {
                CashFlowInstallment = CashFlow.GetInstallmentById(id),
                Logs = CashFlow.GetLogByCashFlowInstallmentId(id)
            };

            return PartialView("DetailsContent", model);
        }

        public ActionResult Delete(int id, bool input)
        {
            var rs = CashFlow.Delete(id);

            if (rs.Successful)
            {
                OpenModal(StateModal.Success, "Sucesso", "Movimentação excluida com sucesso.");
                return RedirectToAction(input ? "CashInput" : "CashOutput", "CashFlow");
            }
            else
            {
                OpenModal(StateModal.Danger, "Erro", rs.ErrorsDetails.First().Message);
                return RedirectToAction(input ? "CashInput" : "CashOutput", "CashFlow");
            }
        }

        public ActionResult CashFlowType()
        {
            ParseCashFlowTypeToView(Core.CashFlow.GetCashFlowType(true, true));
            return View();
        }

        public ActionResult CreateCashFlowType()
        {
            ViewBag.TypeId = new SelectList(Core.CashFlow.GetTypes(), "Id", "Description", 1);
            return View();
        }

        [HttpPost]
        public ActionResult CreateCashFlowType(DTO.ModelsView.CashFlowType cashFlowType)
        {
            if (cashFlowType.Id.Equals(0))
                ModelState.Remove("Id");

            if (ModelState.IsValid)
            {
                var rs = Core.CashFlow.CreateCashFlowType(cashFlowType, UserAuthenticatedId);

                if (rs.Successful)
                {
                    OpenModal(StateModal.Success, "Sucesso", "Salvo com sucesso.");
                    return RedirectToAction("CashFlowType");
                }
                else
                {
                    OpenModal(StateModal.Danger, "Erro", rs.ErrorsDetails.First().Message);
                    return RedirectToAction("CashFlowType");
                }
            }

            OpenModal(StateModal.Warning, "Atenção", "Preencha todos os campos corretamente.");
            return View("CreateCashFlowType", cashFlowType);
        }

        public ActionResult EditCashFlowType(int id)
        {
            var cashFlowType = Core.CashFlow.GetCashFlowTypeById(id);
            ViewBag.TypeId = new SelectList(Core.CashFlow.GetTypes(), "Id", "Description", cashFlowType.TypeId);

            return View("CreateCashFlowType", Core.CashFlow.CashFlowTypeToModelView(cashFlowType));
        }

        public ActionResult DeleteCashFlowType(int id)
        {
            var rs = Core.CashFlow.DeleteCashFlowTypeById(id, UserAuthenticatedId);

            if (rs.Successful)
            {
                OpenModal(StateModal.Success, "Sucesso", "Removido com sucesso.");
                return RedirectToAction("CashFlowType");
            }
            else
            {
                OpenModal(StateModal.Danger, "Erro", rs.ErrorsDetails.First().Message);
                return RedirectToAction("CashFlowType");
            }
        }

        public JsonResult GetAttachments(int cashFlowInstallmentId, string guid)
        {
            var images = new List<DTO.ModelsView.Image>();
            var attachmentsList = CashFlow.GetAttachments(cashFlowInstallmentId).ToList();

            DropzoneFromController(attachmentsList, guid);

            foreach (var file in attachmentsList)
            {
                images.Add(new DTO.ModelsView.Image()
                {
                    Id = file.Id,
                    Name = file.Name,
                    Path = $"Image?id={file.Id}",
                    Size = file.Content.Length
                });
            }

            return new JsonResult { Data = Json(new { Data = images, Guid = guid }), JsonRequestBehavior = JsonRequestBehavior.AllowGet };
        }

        private void ParseCashFlowTypeToView(object obj)
        {
            ViewBag.TableValues = obj;
        }

        private void ParseValuesToTemp(List<ProcessManagementModel.CashFlowInstallments> results, string group = "details")
        {
            var grouping = new Dictionary<string, decimal>();
            var categoriesChart = new List<string>();
            var valuesChart = new List<string>();

            switch (group)
            {
                case "day":
                    foreach (var g in results.GroupBy(r => r.PaymentDate.ToString("dd/MM/yyyy")))
                    {
                        var totalOfDay = g.Select(r => r.ValueInstallment).Sum();

                        grouping[g.Key] = totalOfDay;
                        categoriesChart.Add(g.Key);
                        valuesChart.Add(string.Format("{0:0.00}", totalOfDay));
                    }

                    ViewBag.TableValues = grouping;
                    break;

                case "month":
                    foreach (var g in results.GroupBy(r => r.PaymentDate.Month.ToString()))
                    {
                        var totalOfMonth = g.Select(r => r.ValueInstallment).Sum();

                        grouping[GetNameMonth(g.Key)] = totalOfMonth;
                        categoriesChart.Add(GetNameMonth(g.Key));
                        valuesChart.Add(string.Format("{0:0.00}", totalOfMonth));
                    }

                    ViewBag.TableValues = grouping;
                    break;

                case "year":
                    foreach (var g in results.GroupBy(r => r.PaymentDate.Year.ToString()))
                    {
                        var totalOfYear = g.Select(r => r.ValueInstallment).Sum();

                        grouping[g.Key] = totalOfYear;
                        categoriesChart.Add(g.Key);
                        valuesChart.Add(string.Format("{0:0.00}", totalOfYear));
                    }

                    ViewBag.TableValues = grouping;
                    break;

                default:
                    foreach (var result in results)
                    {
                        categoriesChart.Add(result.PaymentDate.ToString("dd/MM/yyyy"));
                        valuesChart.Add(string.Format("{0:0.00}", result.ValueInstallment));
                    }

                    ViewBag.TableValues = results;
                    break;
            }

            ViewBag.ChartCategories = categoriesChart.ToArray();
            ViewBag.ChartValues = valuesChart.ToArray();
            ViewBag.Group = group;
        }

        public ActionResult Comparative()
        {
            var month = DateTime.Now.Month.ToString().Length.Equals(1) ? $"0{DateTime.Now.Month.ToString()}" : DateTime.Now.Month.ToString();

            var date1 = $"01/{month}/{DateTime.Now.Year}";
            var date2 = $"{DateTime.DaysInMonth(DateTime.Now.Year, DateTime.Now.Month)}/{month}/{DateTime.Now.Year}";

            var inputs = Core.CashFlow.GetCashInputs(date1, date2, UserAuthenticatedId, AccessGroupUser.Id);
            var outputs = Core.CashFlow.GetCashOutputs(date1, date2, UserAuthenticatedId, AccessGroupUser.Id);

            FillComparativeGroupByDay(inputs, "Input", date1, date2);
            FillComparativeGroupByDay(outputs, "Output", date1, date2);
            FillBalance();

            var total = inputs.Select(i => i.ValueInstallment).Sum();
            total += outputs.Select(i => i.ValueInstallment).Sum();

            FillPercent(inputs, "Input", total);
            FillPercent(outputs, "Output", total);

            TempData["Date1"] = date1;
            TempData["Date2"] = date2;

            ViewBag.UserId = UnionSelectList(new SelectList(Core.User.GetUsers(UserAuthenticated.AccessGroup.ClientId, UserAuthenticated.AccessGroupId), "Id", "UserName"));

            return View();
        }

        [HttpPost]
        public ActionResult Comparative(FormCollection form)
        {
            try
            {
                var date1 = form["date1"];
                var date2 = form["date2"];
                var group = form["groupOption"];
                var userId = form["userId"];

                if (ValidateParams(date1, date2, group))
                {
                    var inputs = Core.CashFlow.GetCashInputs(date1, date2, UserAuthenticatedId, AccessGroupUser.Id, userId);
                    var outputs = Core.CashFlow.GetCashOutputs(date1, date2, UserAuthenticatedId, AccessGroupUser.Id, userId);

                    switch (group)
                    {
                        case "day":
                            FillComparativeGroupByDay(inputs, "Input", date1, date2);
                            FillComparativeGroupByDay(outputs, "Output", date1, date2);
                            break;

                        case "month":
                            FillComparativeGroupByMonth(inputs, "Input", date1.Substring(6, 4), Convert.ToInt32(date1.Substring(3, 2)), Convert.ToInt32(date2.Substring(3, 2)));
                            FillComparativeGroupByMonth(outputs, "Output", date1.Substring(6, 4), Convert.ToInt32(date1.Substring(3, 2)), Convert.ToInt32(date2.Substring(3, 2)));
                            break;

                        case "year":
                            FillComparativeGroupByYear(inputs, "Input", Convert.ToInt32(date1.Substring(6, 4)), Convert.ToInt32(date2.Substring(6, 4)));
                            FillComparativeGroupByYear(outputs, "Output", Convert.ToInt32(date1.Substring(6, 4)), Convert.ToInt32(date2.Substring(6, 4)));
                            break;
                    }

                    FillBalance();

                    var total = inputs.Select(i => i.ValueInstallment).Sum();
                    total += outputs.Select(i => i.ValueInstallment).Sum();

                    FillPercent(inputs, "Input", total);
                    FillPercent(outputs, "Output", total);

                    TempData["Date1"] = date1;
                    TempData["Date2"] = date2;

                    ViewBag.UserId = UnionSelectList(new SelectList(Core.User.GetUsers(UserAuthenticated.AccessGroup.ClientId, UserAuthenticated.AccessGroupId), "Id", "UserName", string.IsNullOrEmpty(userId) ? 0 : Convert.ToInt32(userId)));
                }
                else
                {
                    return RedirectToAction("Comparative", "CashFlow");
                }
            }
            catch (Exception ex)
            {
                OpenModal(StateModal.Danger, "Erro", ex.Message);
            }

            return View("Comparative");
        }

        private bool ValidateParams(string date1, string date2, string group)
        {
            if (!group.Equals("year"))
            {
                var paramDate1 = DateTime.ParseExact(date1, "dd/MM/yyyy", new CultureInfo("en-US"));
                var paramDate2 = DateTime.ParseExact(date2, "dd/MM/yyyy", new CultureInfo("en-US"));

                if (paramDate1 > paramDate2)
                {
                    OpenModal(StateModal.Danger, "Atenção", "Data inicial deve ser maior do que a final!");
                    return false;
                }

                if (paramDate1.Year != paramDate2.Year)
                {
                    OpenModal(StateModal.Danger, "Atenção", "Não é possível realizar a busca em anos diferentes!");
                    return false;
                }
            }

            return true;
        }

        private void FillComparativeGroupByDay(List<ProcessManagementModel.CashFlowInstallments> results, string option, string date1, string date2)
        {
            var categoriesChart = new List<string>();
            var valuesChart = new List<string>();

            var isFirstDay = true;

            var firstDay = date1.Substring(0, 2);
            var firstMonth = date1.Substring(3, 2);
            var year = Convert.ToInt32(date1.Substring(6, 4));

            var lastDay = date2.Substring(0, 2);
            var lastMonth = date2.Substring(3, 2);

            for (var iMonth = Convert.ToInt32(firstMonth); iMonth <= Convert.ToInt32(lastMonth); iMonth++)
            {
                var month = SuitSize(iMonth.ToString(), '0', 2, true);

                if (!isFirstDay)
                    firstDay = "1";

                var daysInMonth = (month.Equals(lastMonth)) ? Convert.ToInt32(lastDay) : DateTime.DaysInMonth(year, Convert.ToInt32(month));

                for (int day = Convert.ToInt32(firstDay); day <= daysInMonth; day++)
                {
                    if (results.Any(r => r.PaymentDate.Day.Equals(day) && r.PaymentDate.Month.Equals(Convert.ToInt32(month)) && r.PaymentDate.Year.Equals(year)))
                    {
                        var grouped = results.Where(r => r.PaymentDate.Day.Equals(day) && r.PaymentDate.Month.Equals(Convert.ToInt32(month)) && r.PaymentDate.Year.Equals(year)).GroupBy(r => r.PaymentDate.ToString("dd/MM/yyyy")).FirstOrDefault();

                        var totalValue = grouped.Select(r => r.ValueInstallment).Sum();

                        categoriesChart.Add(grouped.Key);
                        valuesChart.Add(string.Format("{0:0.00}", totalValue));
                    }
                    else
                    {
                        categoriesChart.Add($"{day}/{month}/{year}");
                        valuesChart.Add(string.Format("{0:0.00}", 0));
                    }
                }

                isFirstDay = false;
            }

            TempData[$"ChartCategories"] = categoriesChart.ToArray();
            TempData[$"ChartValues{option}"] = valuesChart.ToArray();
            TempData["Group"] = "day";

            TempData[$"TotalAmount{option}"] = string.Format("{0:0.00}", results.Select(r => r.ValueInstallment).Sum());
        }

        private void FillComparativeGroupByMonth(List<ProcessManagementModel.CashFlowInstallments> results, string option, string year, int monthIndex = 1, int countMonth = 0)
        {
            var categoriesChart = new List<string>();
            var valuesChart = new List<string>();

            for (var i = monthIndex; i <= countMonth; i++)
            {
                if (results.Any(r => r.PaymentDate.Month.Equals(i) && r.PaymentDate.Year.Equals(Convert.ToInt32(year))))
                {
                    var grouped = results.Where(r => r.PaymentDate.Month.Equals(i) && r.PaymentDate.Year.Equals(Convert.ToInt32(year))).GroupBy(r => r.PaymentDate.ToString("MM/yyyy")).FirstOrDefault();

                    var totalOfMonth = grouped.Select(r => r.ValueInstallment).Sum();

                    if (TempData[$"ChartCategories"] == null)
                        categoriesChart.Add(grouped.Key);

                    valuesChart.Add(string.Format("{0:0.00}", totalOfMonth));
                }
                else
                {
                    if (TempData[$"ChartCategories"] == null)
                        categoriesChart.Add($"{SuitSize(i.ToString(), '0', 2, true)}/{year}");

                    valuesChart.Add(string.Format("{0:0.00}", 0));
                }
            }

            if (TempData[$"ChartCategories"] == null)
                TempData[$"ChartCategories"] = categoriesChart.ToArray();

            TempData[$"ChartValues{option}"] = valuesChart.ToArray();
            TempData["Group"] = "month";

            TempData[$"TotalAmount{option}"] = string.Format("{0:0.00}", results.Select(r => r.ValueInstallment).Sum());
        }

        private void FillComparativeGroupByYear(List<ProcessManagementModel.CashFlowInstallments> results, string option, int startYear, int finshYear)
        {
            var categoriesChart = new List<string>();
            var valuesChart = new List<string>();

            for (var year = startYear; year <= finshYear; year++)
            {
                if (results.Any(r => r.PaymentDate.Year.Equals(year)))
                {
                    var grouped = results.Where(r => r.PaymentDate.Year.Equals(year)).GroupBy(r => r.PaymentDate.ToString("yyyy")).FirstOrDefault();

                    var totalOfYear = grouped.Select(g => g.ValueInstallment).Sum();

                    if (TempData[$"ChartCategories"] == null)
                        categoriesChart.Add(grouped.Key);

                    valuesChart.Add(string.Format("{0:0.00}", totalOfYear));
                }
                else
                {
                    if (TempData[$"ChartCategories"] == null)
                        categoriesChart.Add(year.ToString());

                    valuesChart.Add(string.Format("{0:0.00}", 0));
                }
            }

            if (TempData[$"ChartCategories"] == null)
                TempData[$"ChartCategories"] = categoriesChart.ToArray();

            TempData[$"ChartValues{option}"] = valuesChart.ToArray();
            TempData["Group"] = "year";

            TempData[$"TotalAmount{option}"] = string.Format("{0:0.00}", results.Select(r => r.ValueInstallment).Sum());
        }

        private void FillBalance()
        {
            var balanceChart = new List<string>();
            double chartBalanceTotal = 0;

            if (TempData[$"ChartCategories"] != null)
            {
                var categoriesChart = (string[])TempData[$"ChartCategories"];
                var valuesChartInput = (string[])TempData[$"ChartValuesInput"];
                var valuesChartOutput = (string[])TempData[$"ChartValuesOutput"];

                for (var i = 0; i < categoriesChart.Length; i++)
                {
                    var value = (Double.Parse(valuesChartInput[i]) - Double.Parse(valuesChartOutput[i]));
                    chartBalanceTotal += value;
                    balanceChart.Add(string.Format("{0:0.00}", value));
                }

                TempData[$"ChartBalance"] = balanceChart.ToArray();
                TempData[$"ChartBalanceTotal"] = string.Format("{0:0.00}", chartBalanceTotal);
            }
        }

        private void FillPercent(List<ProcessManagementModel.CashFlowInstallments> results, string option, decimal total)
        {
            if (results.Any())
            {
                var value = results.Select(r => r.ValueInstallment).Sum();
                TempData[$"{option}Percent"] = string.Format("{0:0.00}", value * 100 / total);
            }
            else
            {
                TempData[$"{option}Percent"] = string.Format("{0:0.00}", 0);
            }
        }
    }
}