﻿using ProcessManagement.Cache;
using ProcessManagementModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;

namespace ProcessManagement.Controllers
{
    [Authorize]
    public class HomeController : ApplicationController
    {
        public ActionResult Index()
        {
            try
            {
                ViewBag.Menu = CacheManager.GetMenu(AccessGroupUser.Id);
                FillDashboard();
            }
            catch (Exception ex)
            {
                OpenModal(StateModal.Danger, "Erro", ex.Message);
            }

            return View("Dashboard");
        }

        public ActionResult Dashboard()
        {
            FillDashboard();
            return RedirectToAction("Index", "Home");
        }

        public ActionResult LogOff()
        {
            FormsAuthentication.SignOut();
            return RedirectToAction("Index", "Login");
        }

        private void FillDashboard()
        {
            TempData[$"ChartCategories"] = null;

            var resultByYear = Core.CashFlow.GetAllByYear(DateTime.Now.Year, UserAuthenticatedId, AccessGroupUser.Id);
            var date1 = new DateTime(DateTime.Now.Year, 1, 1).ToString("dd/MM/yyyy");
            var date2 = new DateTime(DateTime.Now.Year, 12, DateTime.DaysInMonth(DateTime.Now.Year, 12)).ToString("dd/MM/yyyy");

            FillComparative(Core.CashFlow.GetCashInputs(date1, date2, UserAuthenticatedId, AccessGroupUser.Id), "Input");
            FillComparative(Core.CashFlow.GetCashOutputs(date1, date2, UserAuthenticatedId, AccessGroupUser.Id), "Output");

            FillTotalYear(resultByYear);
            FillResume(resultByYear);
            FillPercent(resultByYear);
        }

        private void FillComparative(List<ProcessManagementModel.CashFlowInstallments> results, string option)
        {
            var categoriesChart = new List<string>();
            var valuesChart = new List<string>();

            for (var i = 1; i <= 12; i++)
            {
                if (results.Any(r => r.PaymentDate.Month.Equals(i)))
                {
                    var grouped = results.Where(r => r.PaymentDate.Month.Equals(i)).GroupBy(r => r.PaymentDate.Month.ToString()).FirstOrDefault();

                    var totalOfMonth = grouped.Select(r => r.ValueInstallment).Sum();

                    if (TempData[$"ChartCategories"] == null)
                        categoriesChart.Add(GetNameMonth(grouped.Key));

                    valuesChart.Add(string.Format("{0:0.00}", totalOfMonth));
                }
                else
                {
                    if (TempData[$"ChartCategories"] == null)
                        categoriesChart.Add(GetNameMonth(i.ToString()));

                    valuesChart.Add(string.Format("{0:0.00}", 0));
                }
            }

            if (TempData[$"ChartCategories"] == null)
                TempData[$"ChartCategories"] = categoriesChart.ToArray();

            TempData[$"ChartValues{option}"] = valuesChart.ToArray();
        }

        private void FillResume(List<ProcessManagementModel.CashFlowInstallments> results)
        {
            if (results.Any())
            {
                TempData[$"InputsTotalResume"] = string.Format("{0:0.00}", results.Where(r => r.CashFlow_CashFlowId.IsInput && r.PaymentDate.Month.Equals(DateTime.Now.Month) && r.CashFlow_CashFlowId.Active).Select(r => r.ValueInstallment).Sum());
                TempData[$"OutputsTotalResume"] = string.Format("{0:0.00}", results.Where(r => !r.CashFlow_CashFlowId.IsInput && r.PaymentDate.Month.Equals(DateTime.Now.Month) && r.CashFlow_CashFlowId.Active).Select(r => r.ValueInstallment).Sum());
                TempData[$"BalanceResume"] = string.Format("{0:0.00}", Decimal.Parse(TempData["InputsTotalResume"].ToString()) - Decimal.Parse(TempData["OutputsTotalResume"].ToString()));
            }
            else
            {
                TempData[$"InputsTotalResume"] = string.Format("{0:0.00}", 0);
                TempData[$"OutputsTotalResume"] = string.Format("{0:0.00}", 0);
                TempData[$"BalanceResume"] = string.Format("{0:0.00}", 0);
            }
        }

        private void FillTotalYear(List<ProcessManagementModel.CashFlowInstallments> results)
        {
            TempData[$"Year"] = DateTime.Now.Year;

            if (results.Any())
            {
                var inputsTotal = results.Where(r => r.CashFlow_CashFlowId.IsInput).Select(r => r.ValueInstallment).Sum();
                var outputsTotal = results.Where(r => !r.CashFlow_CashFlowId.IsInput).Select(r => r.ValueInstallment).Sum();

                TempData[$"InputsTotal"] = string.Format("{0:0.00}", inputsTotal);
                TempData[$"OutputsTotal"] = string.Format("{0:0.00}", outputsTotal);
                TempData[$"Balance"] = string.Format("{0:0.00}", (inputsTotal - outputsTotal));
            }
            else
            {
                TempData[$"InputsTotal"] = string.Format("{0:0.00}", 0);
                TempData[$"OutputsTotal"] = string.Format("{0:0.00}", 0);
                TempData[$"Balance"] = string.Format("{0:0.00}", 0);
            }
        }

        private void FillPercent(List<ProcessManagementModel.CashFlowInstallments> results)
        {
            if (results.Any())
            {
                var total = results.Where(r => r.PaymentDate.Month.Equals(DateTime.Now.Month) && r.CashFlow_CashFlowId.Active).Select(r => r.ValueInstallment).Sum();
                var inputs = results.Where(r => r.PaymentDate.Month.Equals(DateTime.Now.Month) && r.CashFlow_CashFlowId.Active && r.CashFlow_CashFlowId.IsInput).Select(r => r.ValueInstallment).Sum();
                var outputs = results.Where(r => r.PaymentDate.Month.Equals(DateTime.Now.Month) && r.CashFlow_CashFlowId.Active && !r.CashFlow_CashFlowId.IsInput).Select(r => r.ValueInstallment).Sum();

                TempData[$"InputsPercent"] = string.Format("{0:0.00}", inputs * 100 / total);
                TempData[$"OutputsPercent"] = string.Format("{0:0.00}", outputs * 100 / total);
            }
            else
            {
                TempData[$"InputsPercent"] = string.Format("{0:0.00}", 0);
                TempData[$"OutputsPercent"] = string.Format("{0:0.00}", 0);
            }
        }
    }
}