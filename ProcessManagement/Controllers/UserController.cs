﻿using ProcessManagement.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using System.Data.Entity;
using ProcessManagement.Cache;
using System.Web.Script.Serialization;
using System.IO;

namespace ProcessManagement.Controllers
{
    [Authorize]
    public class UserController : ApplicationController
    {
        public ActionResult Users()
        {
            ParseValuesToTemp(Core.User.GetUsers(UserAuthenticated.AccessGroup.Clients.Id, UserAuthenticated.AccessGroupId));
            return View();
        }

        public ActionResult AlterPassword(int id)
        {
            var model = new DTO.ModelsView.AlterPassword() { UserId = id };
            return View(model);
        }

        [HttpPost]
        public ActionResult AlterPassword(DTO.ModelsView.AlterPassword alterPassword)
        {
            if (ModelState.IsValid)
            {
                var rs = Core.User.AlterPassword(alterPassword);

                if (rs.Successful)
                {
                    OpenModal(StateModal.Success, "Sucesso", "Senha alterada com sucesso.");
                    return RedirectToAction("Index", "Home");
                }
                else
                {
                    OpenModal(StateModal.Warning, "Atenção", rs.ErrorsDetails.First().Message);
                    return RedirectToAction("AlterPassword", alterPassword.UserId);
                }
            }
            else
            {
                OpenModal(StateModal.Warning, "Atenção", "Preencha todos os campos corretamente.");
                return View(new DTO.ModelsView.AlterPassword());
            }
        }

        public ActionResult CreateUser()
        {
            ViewBag.AccessGroupId = new SelectList(Core.User.GetAccessGroups(UserAuthenticated.AccessGroup.ClientId), "Id", "Name");
            return View();
        }

        [HttpPost]
        public async Task<ActionResult> CreateUser(DTO.ModelsView.User user, string inputGuid)
        {
            if (user.Id.Equals(0))
                ModelState.Remove("Id");

            if (ModelState.IsValid)
            {
                var image = !string.IsNullOrEmpty(inputGuid) && GetImagesByGuid(inputGuid).FirstOrDefault() != null ? GetImagesByGuid(inputGuid).FirstOrDefault() : new ProcessManagementModel.Images();

                var rs = Core.User.SaveUser(user, image);

                if (rs.Successful)
                {
                    if (!user.Id.Equals(0)) SetCheckUser();
                    OpenModal(StateModal.Success, "Sucesso", "Usuário salvo com sucesso.");
                    return RedirectToAction("Users");
                }
                else
                {
                    OpenModal(StateModal.Warning, "Atenção", rs.ErrorsDetails[0].Message);
                    ViewBag.AccessGroupId = new SelectList(Core.User.GetAccessGroups(UserAuthenticated.AccessGroup.ClientId), "Id", "Name");
                    return View(user);
                }
            }

            OpenModal(StateModal.Warning, "Atenção", "Preencha todos os campos corretamente.");
            return View(user);
        }

        public ActionResult EditUser(int id)
        {
            var model = Core.User.GetUserById(id);

            ViewBag.IsEdit = true;
            ViewBag.AccessGroupId = new SelectList(Core.User.GetAccessGroups(UserAuthenticated.AccessGroup.ClientId), "Id", "Name", model.AccessGroup.Id);

            return View("CreateUser", Core.User.Dettach(model));
        }

        public ActionResult DeleteUser(int id)
        {
            var rs = Core.User.DeleteUser(id);

            if (rs.Successful)
                OpenModal(StateModal.Success, "Sucesso", "Usuário excluido com sucesso.");
            else
                OpenModal(StateModal.Warning, "Atenção", rs.ErrorsDetails[0].Message);

            return RedirectToAction("Users");
        }

        public JsonResult GetProfileImage(int userId, string guid)
        {
            var images = new List<DTO.ModelsView.Image>();
            var profileImage = Core.User.GetProfileImage(userId);

            if (profileImage != null)
            {
                DropzoneFromController(new List<ProcessManagementModel.Images>() { profileImage }, guid);

                images.Add(new DTO.ModelsView.Image()
                {
                    Id = profileImage.Id,
                    Name = profileImage.Name,
                    Path = $"Image?id={profileImage.Id}",
                    Size = profileImage.Content.Length
                });
            }
            else
                images = null;

            return new JsonResult { Data = Json(new { Data = images, Guid = guid }), JsonRequestBehavior = JsonRequestBehavior.AllowGet };
        }

        public ActionResult AccessGroup()
        {
            var viewGroups = new List<DTO.ModelsView.AccessGroup>();
            var groups = Core.User.GetAccessGroups(UserAuthenticated.AccessGroup.ClientId);

            groups.ForEach(g => viewGroups.Add(new DTO.ModelsView.AccessGroup()
            {
                Active = g.Active,
                Id = g.Id,
                Name = g.Name,
                Selected = false
            }));

            TempData["AccessGroups"] = viewGroups;
            TempData["MenuIdsPermission"] = null;
            ParseValuesToTemp(groups);

            return View();
        }

        [HttpPost]
        public ActionResult SaveAccessGroup(int accessGroupId, string accessGroupName, string jMenus, List<string> permission = null, bool authority = false, bool approver = false)
        {
            var menus = new JavaScriptSerializer().Deserialize<Dictionary<string, bool>>(jMenus);

            if (permission == null)
                permission = new List<string>();

            if (string.IsNullOrEmpty(jMenus))
                return RedirectToAction("AccessGroup", "User");

            if (accessGroupId.Equals(0))
                ModelState.Remove("Id");

            if (ModelState.IsValid)
            {
                var group = new ProcessManagementModel.AccessGroup()
                {
                    Active = true,
                    Id = accessGroupId,
                    Name = accessGroupName,
                    AssignResponsability = authority,
                    Approver = approver
                };

                var rs = Core.User.SaveAccessGroup(group, permission, menus.Where(m => m.Value).Select(m => m.Key).ToList(), UserAuthenticated.AccessGroup.Clients.Id);
                if (rs.Successful)
                {
                    CacheManager.GetMenu(group.Id, true);
                    return Json(new { success = true, responseText = "Grupo de acesso salvo com sucesso." }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(new { success = false, responseText = $"Erro ao salvar grupo de acesso. Detalhes: {rs.ErrorsDetails.First().Message}" }, JsonRequestBehavior.AllowGet);
                }
            }

            return Json(new { success = false, responseText = "Erro" }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult EditAccessGroup(int id)
        {
            var viewGroups = new List<DTO.ModelsView.AccessGroup>();
            var viewMenus = new List<DTO.ModelsView.Menu>();

            var model = Core.User.GetAccessGroupById(id);

            ViewBag.MyData = Core.User.MyData(id);
            ViewBag.MyGroup = Core.User.MyGroup(id);

            var groupsSelected = Core.User.GetAccessGroupsPermissions(id);

            foreach (var group in Core.User.GetAccessGroups(UserAuthenticated.AccessGroup.ClientId))
            {
                viewGroups.Add(new DTO.ModelsView.AccessGroup()
                {
                    Id = group.Id,
                    Name = group.Name,
                    Selected = groupsSelected.Any(g => g.Id.Equals(group.Id))
                });
            }

            viewGroups.Remove(viewGroups.Where(v => v.Id.Equals(id)).FirstOrDefault());
            ViewBag.AccessGroups = viewGroups;

            var menusSelected = Core.User.GetMenuPermissions(id);

            var menus = Core.Menu.Load().Where(x => x.Key.Equals(0)).Select(x => x.Value).FirstOrDefault();

            foreach (var mn in menus)
            {
                var menu = new DTO.ModelsView.Menu()
                {
                    Id = mn.Id,
                    Text = mn.Text,
                    Selected = menusSelected.Any(m => m.Id.Equals(mn.Id)),
                    Icon = mn.Icon,
                    Menu_Childs = new List<DTO.ModelsView.Menu>()
                };

                foreach (var child in mn.Menu_Childs)
                {
                    menu.Menu_Childs.Add(new DTO.ModelsView.Menu()
                    {
                        Id = child.Id,
                        Selected = menusSelected.Any(m => m.Id.Equals(child.Id)),
                        Text = child.Text
                    });
                }

                viewMenus.Add(menu);
            }

            ViewBag.Menu = viewMenus;
            return View("EditAccessGroup", model);
        }

        public ActionResult DeleteAccessGroup(int id)
        {
            var rs = Core.User.DeleteAccessGroup(id);

            if (rs.Successful)
                OpenModal(StateModal.Success, "Sucesso", "Grupo de acesso excluido com sucesso.");
            else
                OpenModal(StateModal.Warning, "Atenção", rs.ErrorsDetails[0].Message);

            return RedirectToAction("AccessGroup", "User");
        }

        public ActionResult CreateAccessGroup()
        {
            var viewGroups = new List<DTO.ModelsView.AccessGroup>();
            var groups = Core.User.GetAccessGroups(UserAuthenticated.AccessGroup.ClientId);

            groups.ForEach(g => viewGroups.Add(new DTO.ModelsView.AccessGroup()
            {
                Active = g.Active,
                Id = g.Id,
                Name = g.Name,
                Selected = false
            }));

            ViewBag.AccessGroups = viewGroups;
            ParseValuesToTemp(groups);

            var menus = new List<DTO.ModelsView.Menu>();
            foreach (var m in Core.Menu.Load().Where(x => x.Key.Equals(0)).FirstOrDefault().Value)
            {
                var menu = new DTO.ModelsView.Menu()
                {
                    Id = m.Id,
                    Text = m.Text,
                    Menu_Childs = new List<DTO.ModelsView.Menu>(),
                    Selected = false,
                    Icon = m.Icon
                };

                foreach (var child in m.Menu_Childs)
                {
                    menu.Menu_Childs.Add(new DTO.ModelsView.Menu()
                    {
                        Id = child.Id,
                        Text = child.Text,
                        Selected = false,
                        Icon = child.Icon
                    });
                }

                menus.Add(menu);
            }

            ViewBag.Menu = menus;
            return View();
        }

        private void ParseValuesToTemp(object obj)
        {
            TempData["TableValues"] = obj;
        }
    }
}