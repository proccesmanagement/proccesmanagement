﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace ProcessManagement.Controllers
{
    public class ImageController : ApplicationController
    {
        public async Task<ActionResult> Index(int id)
        {
            var image = Core.Image.GetById(id);

            return File(image.Content, image.ContentType);
        }
    }
}