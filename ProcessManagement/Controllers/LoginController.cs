﻿using ProcessManagement.Core;
using ProcessManagement.DTO.Envelopes;
using ProcessManagementModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;

namespace ProcessManagement.Controllers
{
    public class LoginController : ApplicationController
    {
        public ActionResult Index(string returnUrl)
        {
            LogOut();
            return View();
        }

        private ActionResult RedirectToLocal(string returnUrl)
        {
            if (Url.IsLocalUrl(returnUrl))
                return Redirect(returnUrl);

            return RedirectToAction("Index", "Home");
        }

        [HttpPost]
        public ActionResult SignIn(Users user, string returnUrl)
        {
            var rs = Authentication.SignIn(new SignInRQ() { UserName = user.UserName, Password = user.Password });

            if (rs.Successful)
            {
                FormsAuthentication.SetAuthCookie(rs.User.Id.ToString(), false);
                return RedirectToLocal(returnUrl);
            }

            return RedirectToAction("Index", "Login");
        }

        public ActionResult LogOut()
        {
            FormsAuthentication.SignOut();
            LogOutSessionUser();
            return RedirectToAction("Index", "Login");
        }
    }
}