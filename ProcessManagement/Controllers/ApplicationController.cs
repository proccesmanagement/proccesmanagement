﻿using ProcessManagement.Cache;
using ProcessManagement.Core;
using ProcessManagementModel;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace ProcessManagement.Controllers
{
    public class ApplicationController : CookieController
    {
        public int UserAuthenticatedId => User.Identity.IsAuthenticated ? Convert.ToInt32(User.Identity.Name) : 0;

        public Users UserAuthenticated => Core.User.GetUserById(UserAuthenticatedId);

        public AccessGroup AccessGroupUser => UserAuthenticated.AccessGroup;

        public bool IsApprover => AccessGroupUser.Approver;

        public enum StateModal
        {
            Success,
            Danger,
            Warning,
            Info
        };

        [ChildActionOnly]
        public ActionResult TempMessage()
        {
            return PartialView("CommonModals");
        }

        public async Task SendRegisterEmail(string sendEmail, string name, string subject, string password, string userName, DTO.ModelsView.AccessGroup accessGroup)
        {
            var email = new Core.Email();
            await email.SendRegisterEmail(sendEmail, name, subject, password, userName, accessGroup);
        }

        public void SetCheckUser()
        {
            HttpContext.Session?.Add("UserId", UserAuthenticated.Id);
            HttpContext.Session?.Add("UserName", UserAuthenticated.UserName);
            HttpContext.Session?.Add("Name", UserAuthenticated.Name);
            HttpContext.Session?.Add("ProfileImageId", UserAuthenticated.ProfileImageId.HasValue ? UserAuthenticated.ProfileImageId.Value : 0);
            HttpContext.Session?.Add("ClientDisplayName", UserAuthenticated.AccessGroup.Clients.DisplayName);
            HttpContext.Session?.Add("ProfileContent", Convert.ToBase64String(UserAuthenticated.ProfileImageId.HasValue ? UserAuthenticated.Images.Content : new byte[0]));
        }

        public void LogOutSessionUser()
        {
            HttpContext.Session["UserId"] = null;
            HttpContext.Session["UserName"] = null;
            HttpContext.Session["Name"] = null;
            HttpContext.Session["ProfileImageId"] = null;
            HttpContext.Session["ClientDisplayName"] = null;
        }

        public void LoadMenu()
        {
            ViewBag.Menu = CacheManager.GetMenu(AccessGroupUser.Id);
        }

        public void OpenModal(StateModal stateModal, string title, string text)
        {
            TempData["StateModal"] = stateModal;
            TempData["TitleModal"] = title;
            TempData["TextModal"] = text;
        }

        [HttpPost]
        public JsonResult Dropzone(string guid)
        {
            try
            {
                if (!string.IsNullOrEmpty(guid))
                {
                    if (!CacheManager._cacheImagesAtachment.Any(x => x.Key.Equals(guid)))
                        CacheManager._cacheImagesAtachment.Add(guid, new List<ProcessManagementModel.Images>());

                    for (var i = 0; i < Request.Files.Count; i++)
                    {
                        var file = Request.Files.Get(i);
                        CacheManager._cacheImagesAtachment[guid].Add(RequestFileToImages(file));
                    }
                }

                return Json(new { guid = guid });
            }
            catch (Exception ex)
            {
                return Json(new { messageError = ex.Message + " - " + ex.InnerException });
            }
        }

        public void DropzoneFromController(List<Images> images, string guid)
        {
            if (!string.IsNullOrEmpty(guid))
            {
                if (!CacheManager._cacheImagesAtachment.Any(x => x.Key.Equals(guid)))
                    CacheManager._cacheImagesAtachment.Add(guid, new List<ProcessManagementModel.Images>());

                foreach (var image in images)
                    if (!CacheManager._cacheImagesAtachment[guid].Any(x => x.Content.Equals(image.ContentType)))
                        CacheManager._cacheImagesAtachment[guid].Add(image);
            }
        }

        private ProcessManagementModel.Images RequestFileToImages(HttpPostedFileBase file)
        {
            var image = new ProcessManagementModel.Images();

            image.Name = Path.GetFileName(file.FileName);
            image.ContentType = file.ContentType;

            using (var reader = new BinaryReader(file.InputStream))
                image.Content = reader.ReadBytes(file.ContentLength);

            return image;
        }

        [HttpPost]
        public JsonResult RemoveFileFromCache(string guid, string fileName)
        {
            try
            {
                if (!string.IsNullOrEmpty(guid) && !string.IsNullOrEmpty(fileName))
                {
                    if (CacheManager._cacheImagesAtachment.Any(c => c.Key.Equals(guid)))
                    {
                        var cache = new List<ProcessManagementModel.Images>();
                        cache = CacheManager._cacheImagesAtachment[guid].Where(x => x.Name.Equals(fileName)).ToList();

                        foreach (var file in cache)
                            CacheManager._cacheImagesAtachment[guid].Remove(file);
                    }
                }

                return Json(new { message = "Success" });
            }
            catch (Exception ex)
            {
                return Json(new { messageError = ex.Message + " - " + ex.InnerException });
            }
        }

        public List<ProcessManagementModel.Images> GetImagesByGuid(string guid)
        {
            var images = new List<ProcessManagementModel.Images>();

            if (CacheManager._cacheImagesAtachment.Any(c => c.Key.Equals(guid)))
                images = CacheManager._cacheImagesAtachment[guid];

            return images;
        }

        public List<SelectListItem> UnionSelectList(SelectList selectList)
        {
            var all = new Dictionary<int, string>() { { 0, "Todos" } };
            var selectListWithFieldAll = new SelectList(all, "Key", "Value", 0);

            return selectListWithFieldAll.Union(selectList).ToList();
        }

        public string SuitSize(string value, char character, int length, bool left)
        {
            if (value.Length < length)
            {
                while (value.Length < length)
                {
                    if (left)
                        value = $"{character}{value}";
                    else
                        value = $"{value}{character}";
                }
            }
            else if (value.Length > length)
            {
                if (left)
                    value = value.Substring(0, length);
                else
                    value = value.Substring(value.Length - length, length);
            }

            return value.ToUpper();
        }

        public string GetNameMonth(string month)
        {
            switch (month)
            {
                case "1":
                    return "Janeiro";

                case "2":
                    return "Fevereiro";

                case "3":
                    return "Marco";

                case "4":
                    return "Abril";

                case "5":
                    return "Maio";

                case "6":
                    return "Junho";

                case "7":
                    return "Julho";

                case "8":
                    return "Agosto";

                case "9":
                    return "Setembro";

                case "10":
                    return "Outubro";

                case "11":
                    return "Novembro";

                case "12":
                    return "Dezembro";

                default:
                    return "";
            }
        }
    }
}