﻿using System.Web;
using System.Web.Optimization;

namespace ProcessManagement
{
    public class BundleConfig
    {
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new StyleBundle("~/Content/css").Include(
                "~/assets/stylesheets/bootstrap.min.css",
                "~/assets/stylesheets/pixel-admin.css",
                "~/assets/stylesheets/widgets.css",
                "~/assets/stylesheets/rtl.css",
                "~/assets/stylesheets/themes.min.css",
                "~/assets/stylesheets/jquery.toolbar.css"
                ));

            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                        "~/Scripts/Common/jquery-{version}.js"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
                        "~/Scripts/Common/jquery.validate*"));

            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                        "~/Scripts/Common/modernizr-*"));

            bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
                      "~/Scripts/Common/bootstrap.js",
                      "~/Scripts/Pixel-Admin/pixel-admin.js",
                      "~/Scripts/Pixel-Admin/pixel-admin2.min.js",
                      "~/Scripts/Common/respond.js"));

            bundles.Add(new ScriptBundle("~/bundles/global").Include(
                        "~/Scripts/Service/ServiceCaller.js",
                        "~/Scripts/Common/Global.js",
                        "~/Scripts/Common/json2.min.js",
                        "~/Scripts/Common/Util.js"));

            bundles.Add(new ScriptBundle("~/bundles/home").Include(
                        "~/Scripts/Views/Home.js"));

            bundles.Add(new ScriptBundle("~/bundles/datePickerJs").Include(
                "~/Scripts/DatePicker/bootstrap-datepicker.js"));

            bundles.Add(new ScriptBundle("~/bundles/maskMoney").Include(
                        "~/Scripts/MaskMoney/maskMoney.js"));

            bundles.Add(new ScriptBundle("~/bundles/spin").Include(
                        "~/Scripts/Spin/spin.js"));

            bundles.Add(new ScriptBundle("~/bundles/views").Include(
                        "~/Scripts/Views/CashFlow/CreateInput.js"));

            bundles.Add(new ScriptBundle("~/bundles/table").Include(
                        "~/Scripts/bootstrap-table/bootstrap-table.js",
                        "~/Scripts/bootstrap-table/extensions/filter/bootstrap-table-filter.js",
                        "~/Scripts/bootstrap-table/extensions/cookie/bootstrap-table-cookie.js",
                        "~/Scripts/bootstrap-table/extensions/flatJSON/bootstrap-table-flatJSON.js",
                        "~/Scripts/bootstrap-table/extensions/mobile/bootstrap-table-mobile.js",
                        "~/Scripts/bootstrap-table/extensions/resizable/bootstrap-table-resizable.js",
                        "~/Scripts/bootstrap-table/extensions/export/jquery.base64.js",
                        "~/Scripts/bootstrap-table/extensions/export/tableExport.js",
                        "~/Scripts/bootstrap-table/extensions/export/bootstrap-table-export.js",
                        "~/Scripts/Common/jquery.toolbar.js"));

            bundles.Add(new ScriptBundle("~/bundles/charts").Include(
                        "~/Scripts/Charts/Chart.js"));

            bundles.Add(new StyleBundle("~/Content/table").Include(
                   "~/assets/stylesheets/bootstrap-table.css"));

            bundles.Add(new StyleBundle("~/Content/datePickerCss").Include(
                    "~/Content/DatePicker/css/datepicker.css"));

            bundles.Add(new StyleBundle("~/Content/bootstrapSys").Include(
                    "~/Content/bootstrap.min.css",
                    "~/Content/bootstrapSys.css",
                    "~/Content/Pixel-Admin/pixel-admin.css",
                    "~/Scripts/Common/respond.js"));

            bundles.Add(new StyleBundle("~/Content/styleSite").Include(
                      "~/Content/styles.css"));
        }
    }
}
