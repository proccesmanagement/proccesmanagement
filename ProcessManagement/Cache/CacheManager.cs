﻿using ProcessManagement.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ProcessManagement.Cache
{
    public static class CacheManager
    {
        internal static Dictionary<int, List<DTO.CacheModels.Menu>> _cacheMenu = new Dictionary<int, List<DTO.CacheModels.Menu>>();

        internal static Dictionary<string, List<ProcessManagementModel.Images>> _cacheImagesAtachment = new Dictionary<string, List<ProcessManagementModel.Images>>();

        public static List<DTO.CacheModels.Menu> GetMenu(int? accessGroupId = null, bool force = false)
        {
            if (((accessGroupId.HasValue && !_cacheMenu.Any(x => x.Key.Equals(accessGroupId.Value))) || (!accessGroupId.HasValue)) || force)
                _cacheMenu[accessGroupId.Value] = Menu.Load(accessGroupId).Select(x => x.Value).FirstOrDefault();

            return _cacheMenu.Where(c => c.Key.Equals(accessGroupId.Value)).FirstOrDefault().Value;
        }
    }
}