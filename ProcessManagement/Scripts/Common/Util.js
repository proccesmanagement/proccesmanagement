﻿function setCookie(name, value, days) {
    var expires;
    var date;
    var value;

    date = new Date();
    date.setTime(date.getTime() + (days * 24 * 60 * 60 * 1000));

    expires = date.toUTCString();

    document.cookie = name + "=" + value + "; expires=" + expires + "; path=/";
}

function getCookie(name) {
    var nameEQ = name + "=";
    var ca = document.cookie.split(';');

    for (var i = 0; i < ca.length; i++) {
        var c = ca[i];

        while (c.charAt(0) == ' ')
            c = c.substring(1, c.length);

        if (c.indexOf(nameEQ) == 0)
            return c.substring(nameEQ.length, c.length);
    }

    return null;
}

function dateFormat(date) {
    var dd = date.getDate();
    var mm = date.getMonth() + 1; //Janeiro == 0!

    var yyyy = date.getFullYear();

    if (dd < 10) {
        dd = '0' + dd
    }

    if (mm < 10) {
        mm = '0' + mm
    }

   return dd + '/' + mm + '/' + yyyy;
}

function removeCookie(name) {
    setCookie(name, "", -1)
}

function clearSettings() {
    settings.components = {};
}

function clearInputsModal() {
    $('.modal-body input').val('');
}

function openModal(url, idContent, idModal, id, input) {
    var dataRequest = {};

    if (id)
        dataRequest.id = id;

    if (input === true || input === false)
        dataRequest.input = input;

    $.ajax({
        type: "GET",
        url: url,
        data: dataRequest,
        success: function (data) {
            $(idContent).html(data);
            $(idModal).modal('show');
        }
    })
}

function startdatepickers() {
    $(".datepicker").datepicker({
        autoclose: true,
        format: "dd/mm/yyyy",
        enableOnReadonly: false,
        language: "pt"
    });

    $('.datepicker').removeAttr("data-val-date");
}

function onFileSelected(event) {
    var selectedFile = event.target.files[0];
    var reader = new FileReader();

    if (selectedFile) {
        var imgtag = document.getElementById("myimage");
        imgtag.title = selectedFile.name;

        reader.onload = function (event) {
            imgtag.src = event.target.result;
        };

        reader.readAsDataURL(selectedFile);
    }
    else {
        document.getElementById('myimage').src = '@Href("~/Content/Images/avatar.png")';
    }
}

function clearInputImage() {
    $('#profileImage').val("");
    var imgtag = document.getElementById("myimage");
    imgtag.src = "";
    imgtag.title = "";
};

function guid() {
    function s4() {
        return Math.floor((1 + Math.random()) * 0x10000).toString(16).substring(1);
    }

    return s4() + s4() + '-' + s4() + '-' + s4() + '-' + s4() + '-' + s4() + s4() + s4();
}
