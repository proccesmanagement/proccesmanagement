﻿function loadLineChart(titletext, xAxiscategories, series, chartid) {

    var datasets = [];
    for (var serie = 0; serie < series.length; serie++) {
        var dataset = {};
        dataset['label']= series[serie].name,
        dataset['fill']= false,
        dataset['lineTension']= 0.1,
        dataset['backgroundColor'] = series[serie].color,
        dataset['borderColor'] = series[serie].color,
        dataset['borderCapStyle']= 'butt',
        dataset['borderDash']= [],
        dataset['borderDashOffset']= 0.0,
        dataset['borderJoinStyle']= 'miter',
        dataset['pointBorderColor'] = series[serie].color,
        dataset['pointBackgroundColor']= "#fff",
        dataset['pointBorderWidth']= 1,
        dataset['pointHoverRadius']= 5,
        dataset['pointHoverBackgroundColor'] = series[serie].color,
        dataset['pointHoverBorderColor'] = series[serie].color,
        dataset['pointHoverBorderWidth']= 2,
        dataset['pointRadius']= 1,
        dataset['pointHitRadius']= 10,
        dataset['data'] = series[serie].data,
        dataset['spanGaps']= false
        
        datasets.push(dataset);
    }

    var data = {
        labels: xAxiscategories,
        datasets: datasets
    };

    var myLineChart = new Chart($(chartid), {
        type: 'line',
        data: data,
        options: {
            title: {
                display: true,
                text: titletext
            }
        }
    });
};