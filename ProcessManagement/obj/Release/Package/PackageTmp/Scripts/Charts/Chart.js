﻿function loadhighcharts(titletext, subtitletext, xAxiscategories, yAxistitletext, series, chartid) {
    $(chartid).highcharts({
        chart: {
            type: 'areaspline'
        },
        title: {
            text: titletext,
            x: -20
        },
        subtitle: {
            text: subtitletext,
            x: -20
        },
        xAxis: {
            categories: xAxiscategories
        },
        yAxis: {
            title: {
                text: yAxistitletext
            },
            plotLines: [{
                value: 0,
                width: 1,
                color: '#808080'
            }]
        },
        legend: {
            layout: 'vertical',
            align: 'right',
            verticalAlign: 'middle',
            borderWidth: 0
        },
        plotOptions: {
            series: {
                lineWidth: 1
            }
        },
        tooltip: {
            headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
            pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                '<td style="padding:0"><b>{point.y:.2f}</b></td></tr>',
            footerFormat: '</table>',
            shared: true,
            useHTML: true
        },
        series: series
    });
};

function loadhighchartsBasicLine(titletext, subtitletext, xAxiscategories, yAxistitletext, series, chartid) {
    $(chartid).highcharts({
        title: {
            text: titletext,
            x: -20
        },
        subtitle: {
            text: subtitletext,
            x: -20
        },
        xAxis: {
            categories: xAxiscategories
        },
        yAxis: {
            title: {
                text: yAxistitletext
            },
            plotLines: [{
                value: 0,
                width: 1,
                color: '#808080'
            }]
        },
        legend: {
            layout: 'vertical',
            align: 'right',
            verticalAlign: 'middle',
            borderWidth: 0
        },
        plotOptions: {
            series: {
                lineWidth: 2
            }
        },
        tooltip: {
            headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
            pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                '<td style="padding:0"><b>{point.y:.2f}</b></td></tr>',
            footerFormat: '</table>',
            shared: true,
            useHTML: true
        },
        series: series
    });
};

function loadhighchartsColumn(titletext, subtitletext, xAxiscategories, yAxistitletext, series, chartid) {
    $(chartid).highcharts({
        chart: {
            type: 'column'
        },
        title: {
            text: titletext
        },
        subtitle: {
            text: subtitletext
        },
        xAxis: {
            categories: xAxiscategories
        },
        yAxis: {
            title: {
                text: yAxistitletext
            }
        },
        credits: {
            enabled: false
        },
        tooltip: {
            headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
            pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                '<td style="padding:0"><b>{point.y:.2f}</b></td></tr>',
            footerFormat: '</table>',
            shared: true,
            useHTML: true
        },
        plotOptions: {
            column: {
                pointPadding: 0.2,
                borderWidth: 0
            }
        },
        series:series
    });
};