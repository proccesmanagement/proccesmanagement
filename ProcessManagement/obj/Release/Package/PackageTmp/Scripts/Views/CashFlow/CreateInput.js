﻿function eventsCashFlow() {
    $("#TotalAmount").removeAttr("data-val-number");
    $("#ValueInstallments").removeAttr("data-val-number");
    $("#Id").removeAttr("data-val-required");

    if ($("#TotalAmount").val() == "")
        $("#TotalAmount").val(0)
    else
        $("#TotalAmount").val($("#TotalAmount").val().replace(".", ","))

    if ($("#ValueInstallments").val()) {
        if ($("#ValueInstallments").val() == "")
            $("#ValueInstallments").val(0)
        else
            $("#ValueInstallments").val($("#ValueInstallments").val().replace(".", ","))
    }

    if ($("#Installments").val() == "")
        $("#Installments").val(1)

    $("#TotalAmount").blur(function () {
        calcValueInstallments();
    });

    $("#Installments").blur(function () {
        calcValueInstallments();
    });

    $("#ValueInstallments").blur(function () {
        calcTotalAmount();
    });

    $("#UserId").select2({ placeholder: "Atribua a responsabilidade." });
}

function calcValueInstallments() {
    if (!$("#Installments").val())
        $("#Installments").val(1);

    if (!$("#TotalAmount").val())
        $("#TotalAmount").val(0)

    var valueInstallments = (parseFloat($("#TotalAmount").val().replace(',', '.')) / parseFloat($("#Installments").val().replace(',', '.'))).toFixed(2);
    $("#ValueInstallments").val(valueInstallments.replace('.',','));
}

function calcTotalAmount() {
    if (!$("#Installments").val())
        $("#Installments").val(1);

    if (!$("#ValueInstallments").val())
        $("#ValueInstallments").val(0)

    var totalAmount = (parseFloat($("#ValueInstallments").val().replace(',', '.')) * parseFloat($("#Installments").val().replace(',', '.'))).toFixed(2);
    $("#TotalAmount").val(totalAmount.replace('.', ','));
}