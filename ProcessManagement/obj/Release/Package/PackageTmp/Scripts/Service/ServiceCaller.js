﻿function ServiceCaller(type, method, data, succeeded, completed) {
    var url = Global.UrlWcf + method;

    var json = JSON2.stringifyWcf(data);

    $.ajax({
        type: type,
        url: url,
        data: json,
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        processdata: true,
        success: function (response) {
            if (succeeded) {
                succeeded(response);
            }
        },
        error: ErrorServiceCaller
    });
}

function ErrorServiceCaller(response) {
    ViewCaller("Administrator", "Error", response)
}

function ControllerCaller(controller, action, data) {
    var url = '/' + Global.NameSite + '/' + controller + '/' + action+'';

    var json = JSON2.stringifyWcf(data);

    if (!json)
        json = {};

    $.ajax({
        url: url,
        type: 'GET',
        dataType: 'json',
        cache: false,
        data: json
    });

    window.location = url;
}

function ViewCaller(controller, action, data) {
    var url = '/' + Global.NameSite + '/' + controller + '/' + action + '';

    window.location.href = url;
}

